#version 330 core

uniform mat4 modelViewProjectionMatrix;
uniform mat4 lightBMVP;

in vec3 position;
in vec2 texCoords;

out vec4 vertex;
out vec4 shadow_proj_coords;
out vec4 proj_coords;
out vec2 textureCoords;



void main(){ 
    vertex = vec4(position, 1.0);
    shadow_proj_coords = lightBMVP * vertex;
    textureCoords = texCoords;
    gl_Position = modelViewProjectionMatrix * vertex;
    proj_coords = modelViewProjectionMatrix * vertex;
}
