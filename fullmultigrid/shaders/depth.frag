#version 330 core

// Ouput data
out vec4 fragmentdepth;

void main(void) {
    fragmentdepth = vec4(gl_FragCoord.z);
}
