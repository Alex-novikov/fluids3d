﻿#version 330 core


uniform sampler2D normalMap;
uniform sampler2D refract_texture;
uniform sampler2D reflect_texture;
uniform sampler2D depthMap;
uniform sampler2D shadow_texture;
uniform samplerCube cubemapTexture;

uniform vec4 time_density_clipplane;
uniform vec4 lightDir;
uniform vec3 viewPosition;
uniform vec4 water_color;


in vec4 vertex;
in vec4 shadow_proj_coords;
in vec4 proj_coords;
in vec2 textureCoords;

const float fNormalScale = 3.0;

out vec4 color;


float SampleShadow(in vec3 shadow_tc){
    return textureProj(shadow_texture, shadow_tc);
}

float calculate_linear_depth(in float value){
  return time_density_clipplane.w * time_density_clipplane.z / 
    ( time_density_clipplane.w - value * 
    (time_density_clipplane.w - time_density_clipplane.z) );
}


void main(){
   vec3 normal = 2.0 * texture2D(normalMap, textureCoords * fNormalScale 
  + vec2(time_density_clipplane.x)/500 ).xzy - vec3(1.0);
  // переводим значения карты нормалей в мировое пространство
  normal = normalize(normal + vec3(0.0, 1.0, 0.0));
  // немного «выпрямим» нормаль

  // введем переменную для обозначения
  // масштабного коэффициента HDR изображения
  float fHDRscale = water_color.w;
  // вычислим нормированное положение источника света
  vec3 lpnorm   = normalize(lightDir.xyz);
  // вычислим нормированный вектор взгляда
  vec3 vpnorm   = normalize(viewPosition - vertex.xyz);
  // вычислим проективные координаты
  vec3 proj_tc  = 0.5 * proj_coords.xyz / proj_coords.w + 0.5;
  // вычислим коэффициент Френеля для смешивания отражения и преломления
  float fresnel = 1.0 - dot(vpnorm, normal);

  // вычисляем расстояние от камеры до точки 
 float fOwnDepth = proj_tc.z;
 // считываем глубину сцены
 float fSampledDepth = texture2D(depthMap, proj_tc.xy).x;
 // преобразуем её в линейную (расстояние от камеры)
 fSampledDepth       = calculate_linear_depth(fSampledDepth);
 // получаем линейную глубину воды
 float fLinearDepth  = fSampledDepth - fOwnDepth;

  float fExpDepth = 1.0 - exp( -time_density_clipplane.y * fLinearDepth);
 float fExpDepthHIGH = 1.0 - exp( -0.95 * fLinearDepth );

vec3 shadow_tc = shadow_proj_coords.xyz / shadow_proj_coords.w + 
    0.06 * vec3(normal.x, normal.z, 0.0);
  // вычисляем текстурные координаты со смещением
  float fShadow = SampleShadow(shadow_tc); // получаем непосредственную тень
  float fSurfaceShadow = 0.25 * fShadow + 0.75; // осветляем её

  vec3 ref_vec = reflect(-lpnorm, normal); // вычисляем отраженный вектор
 // получаем скалярное произведение 
 // отраженного вектора на вектор взгляда
 float VdotR = max( dot(ref_vec, vpnorm), 0.0 );  
 // аппроксимация Шлика a^b = a / (b – a*b + a) для a от нуля до единицы
 VdotR /= 1024.0 - VdotR * 1024.0 + VdotR;
 vec3 specular = vec3(VdotR) * fExpDepthHIGH * fShadow; 
 // вычисляем отраженный свет, с учетом затенения и глубины воды в данной точке.

  // величина искажения – чем глубже, тем искажения больше
  float fDistortScale = 0.1 * fExpDepth;
  vec2 vDistort = normal.zx * fDistortScale; // смещение текстурных координат
  // читаем глубину в искаженных координатах
  float fDistortedDepth = texture2D(depthMap, proj_tc.xy + vDistort).x;
  // преобразуем её в линейную
  fDistortedDepth = calculate_linear_depth(fDistortedDepth);
  float fDistortedExpDepth = 
    1.0 - exp( -time_density_clipplane.y * (fDistortedDepth - fOwnDepth) );
  // вычисляем экспоненциальную глубину в искаженных координатах
  // теперь сравниваем расстояния – если расстояние до воды больше,
  // чем до прочитанной то пренебрегаем искажением
  if (fOwnDepth > fDistortedDepth) 
  {
    vDistort = vec2(0.0);
    fDistortedExpDepth = fExpDepth;
  }
  // теперь читаем из текстуры преломлений цвет
  vec3 refraction = texture2D(refract_texture, proj_tc.st + vDistort).xyz;
  // и закрашиваем его цветом воды, в зависимости от глубины
  refraction = mix(refraction, water_color.xyz * fHDRscale, fDistortedExpDepth);

// коэффициент можно подобрать под свои нужды
 vDistort = normal.xz * 0.025;
 vec3 reflection = texture2D(reflect_texture, proj_tc.st + vDistort).xyz;

 float fMix = fresnel * fExpDepthHIGH; 
  // вычисляем коэффициент смешивания
  // как коэффициент Френеля плюс вычисленная глубина
  // для плавного перехода из «берега» в воду
  // смешиваем и умножаем на осветленную тень
  vec3 result_color = mix(refraction, reflection, fMix) * fSurfaceShadow;
  result_color += specular; // добавляем отраженный свет
  color = vec4( result_color, 1.0); // ура! записываем значение


  }
