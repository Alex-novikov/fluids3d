#version 330 core

in vec3 position;
out vec4 pos;

uniform mat4 lightBMVP;
uniform	vec3 viewPosition;                             // позиция камеры в мировой системе координат
uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 modelMatrix;
uniform mat3 normalMatrix;
uniform mat3 worldNormalMatrix;

void main(void){

    gl_Position     =  modelViewProjectionMatrix * vec4(position, 1.0);
    pos = vec4(position, 1.0);
}
