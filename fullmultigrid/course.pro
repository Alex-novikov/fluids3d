include( common/common.pri )

TEMPLATE = app

INCLUDEPATH += common

SOURCES  += main.cpp \
            window.cpp \
            waterscene.cpp \
    glwidget.cpp \
    dialog.cpp


HEADERS  += window.h \
            waterscene.h \
    staticConsts.h \
    common/mesh.h \
    glwidget.h \
    dialog.h

OTHER_FILES +=    shaders/lightning.vert \
    shaders/lightning.frag \
QMAKE_LIBS+=-static -lgomp
LIBS += -fopenmp
QMAKE_LFLAGS += -fopenmp
QMAKE_CXXFLAGS += -fopenmp

RESOURCES += \
    cource.qrc

FORMS += \
    dialog.ui
