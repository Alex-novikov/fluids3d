#ifndef MESH_H
#define MESH_H

#include <map>
#include <lib3ds.h>
#include <QVector3D>
#include <submesh.h>
#include <material.h>

class Model3DS;

using namespace std;
class Mesh{

    Model3DS* model;
    Lib3dsMesh* mesh;
    map<Material*, SubMesh*> submeshes;
    Lib3dsVector* normals;
public:
    Mesh(Model3DS* model, Lib3dsMesh* mesh);
    SubMesh* getSubMesh(Material* mat);
};

#endif // MESH_H
