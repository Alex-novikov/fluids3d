#include "mesh.h"
#include "model3ds.h"

Mesh::Mesh(Model3DS* model, Lib3dsMesh* mesh):
    model(model),
    mesh(mesh){
    // Compute normals for the mesh
    normals = new Lib3dsVector[3*mesh->nfaces];
    lib3ds_mesh_calculate_vertex_normals(mesh, normals);

    // Set up the submeshses
    Lib3dsFile* file = model->file;
    int index = 0;
    for (int j = 0; j < mesh->nfaces; j++) {
        Lib3dsFace* face = &mesh->faces[j];
        Material* mat = static_cast<Material*>(file->materials[face->material]->user_ptr);
        SubMesh* submesh = getSubMesh(mat);

        // Set the normals and the faces
        for (int k = 0; k < 3; k++) {

            // Calculate binormal and tangent

            float* temp = mesh->vertices[face->index[k]];
            QVector3D p0(temp[1],temp[2],temp[3]);
            temp= mesh->vertices[face->index[(k+1)%3]];
            QVector3D p1(temp[1],temp[2],temp[3]);
            temp = mesh->vertices[face->index[(k+2)%3]];
            QVector3D p2(temp[1],temp[2],temp[3]);

            QVector3D d1 = p1-p0;
            QVector3D d2 = p2-p0;

            float u0 = mesh->texcos[face->index[k]][0];
            float v0 = mesh->texcos[face->index[k]][1];
            float u1 = mesh->texcos[face->index[(k+1)%3]][0];
            float v1 = mesh->texcos[face->index[(k+1)%3]][1];
            float u2 = mesh->texcos[face->index[(k+2)%3]][0];
            float v2 = mesh->texcos[face->index[(k+2)%3]][1];

            float s1 = u1-u0;
            float t1 = v1-v0;
            float s2 = u2-u0;
            float t2 = v2-v0;

            float a = 1 / (s1*t2 - s2*t1);
            if (fabs(a)< 0.0001)
                a = 1;
            QVector3D t = a*(d1*t2 - d2*t1).normalized();
            QVector3D b = a*(-d1*s2 + d2*s1).normalized();

            submesh->normal(normals[3*j + k]);
            submesh->tangent(t);
            submesh->binormal(b);
            submesh->texcoord(mesh->texcos[face->index[k]]);
            submesh->vertex(mesh->vertices[face->index[k]]);
        }
    }

    for (map<Material*, SubMesh*>::iterator i = submeshes.begin(); i != submeshes.end(); i++) {
        i->second->init();
    }

    delete[] normals;
}

SubMesh* Mesh::getSubMesh(Material* mat) {

    map<Material*, SubMesh*>::iterator i = submeshes.find(mat);
    if (i == submeshes.end())
        return submeshes.insert(std::pair<Material*, SubMesh*>(mat, new SubMesh(model, mat))).first->second;
    else
        return i->second;

}
