#include <QApplication>

#include "window.h"

#include <QDebug>

int main( int argc, char* argv[] )
{
    QApplication a( argc, argv );
    Window w;
    w.show();
    return a.exec();
}
