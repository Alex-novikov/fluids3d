#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include "dialog.h"

class QSlider;

class GLWidget;

class Window : public QWidget
{
    Q_OBJECT

public:
    Window();
    ~Window();

protected:
    void keyPressEvent(QKeyEvent *event);

private:

    int x,y,t;
    double alpha;

    QSlider *createSlider();

    Dialog *dlg;
    GLWidget *glWidget;
    QSlider *timeSlider;
    double *** data;
};

#endif
