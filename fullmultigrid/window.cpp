#include <QtWidgets>

#include "omp.h"
#include "glwidget.h"
#include "window.h"
#include "math.h"
#include "QTimer"


double inline getBounds(double x, double y, double t, double maxX, double maxY){
    maxX--;
    maxY--;

    if(x==0)
        return 1-2*y/maxY;
    if(y==0)
        return 1-2*x/maxX;
    if(x == maxX)
        return -1+2*y/maxY;
    if(y == maxY)
        return -1+2*x/maxX;
    qDebug() << "wrong";
    return 0;
    //return x/maxX*x/maxX*x/maxX*y/maxY*y/maxY*y/maxY;
}

void inline clearLayer(double **layer, int n, int step, int x, int y){

    for (int i = 0; i < x/step+2; i++){
        for (int j = 0; j < x/step+2; j++){
            layer[n][((int)pow(2,n)+2)*i+j] = 0;
        }
    }
}
void inline applyZeroBounds(double **layer, int n, int step, int x, int y){

    //расставляем граничные условия
    layer[n][0] = 0;
    layer[n][y/step+1] = 0;
    for (int i = 1; i < x/step+1; i++){
        layer[n][((int)pow(2,n)+2)*i] = 0;
        layer[n][((int)pow(2,n)+2)*i+y/step+1] = 0;
    }
    layer[n][((int)pow(2,n)+2)*(x/step+1)] = 0;
    layer[n][((int)pow(2,n)+2)*(x/step+1)+y/step+1] = 0;


    for (int j = 1; j < y/step+1; j++){
        layer[n][j] =  0;
        layer[n][((int)pow(2,n)+2)*(x/step+1)+j] = 0;
    }
}


void inline applyBounds(double **layer, int n, int step, int x, int y){

    //расставляем граничные условия
    layer[n][0] = getBounds(0,0,0,x/step+2,y/step+2);
    layer[n][y/step+1] = getBounds(0,y/step+1,0,x/step+2,y/step+2);

    for (int i = 1; i < x/step+1; i++){
        layer[n][((int)pow(2,n)+2)*i] = getBounds(i,0,0,x/step+1,y/step+2);
        layer[n][((int)pow(2,n)+2)*i+y/step+1] = getBounds(i,y/step+1,0,x/step+2,y/step+2);
    }
    layer[n][((int)pow(2,n)+2)*(x/step+1)] = getBounds(x/step+1,0,0,x/step+2,y/step+2);
    layer[n][((int)pow(2,n)+2)*(x/step+1)+y/step+1] = getBounds(x/step+1,y/step+1,0,x/step+2,y/step+2);

    for (int j = 1; j < y/step+1; j++){
        layer[n][j] =  getBounds(0,j,0,x/step+2,y/step+2);
        layer[n][((int)pow(2,n)+2)*(x/step+1)+j] = getBounds(x/step+1,j,0,x/step+2,y/step+2);
    }
}


void inline applyDataBounds(double ***layer, int n, int step, int x, int y){

    //расставляем граничные условия
    layer[0][0][n] = getBounds(0,0,0,x/step+2,y/step+2);
    layer[0][y/step+1][n] = getBounds(0,y/step+1,0,x/step+2,y/step+2);

    for (int i = 1; i < x/step+1; i++){
        layer[i][0][n] = getBounds(i,0,0,x/step+2,y/step+2);
        layer[i][y/step+1][n] = getBounds(i,y/step+1,0,x/step+2,y/step+2);
    }
    layer[x/step+1][0][n] = getBounds(x/step+1,0,0,x/step+2,y/step+2);
    layer[x/step+1][y/step+1][n] = getBounds(x/step+1,y/step+1,0,x/step+2,y/step+2);

    for (int j = 1; j < y/step+1; j++){
        layer[0][j][n] =  getBounds(0,j,0,x/step+2,y/step+2);
        layer[x/step+1][j][n] = getBounds(x/step+1,j,0,x/step+2,y/step+2);
    }
}

double inline f(double x,double y, double xMax, double yMax){
    return 0;
    //return -6*(x/xMax*x/xMax*y/yMax*(x/xMax*x/xMax+2*y/yMax*y/yMax));
}

void inline coastestGridIteration(double **correction, int step, int x, int y){
    applyBounds(correction, 2, step, x, y);
    for(int n = 0;n < 3; n++){
        for(int j = 1;j < y/step+1;j++)
            for(int i = 1;i < x/step+1;i++)
                correction[2][i*6 + j]=0.25*(correction[2][(i-1)*6+j] + correction[2][(i+1)*6+j]
                        +correction[2][i*6+j-1]+correction[2][i*6+j+1]+((double)step*step/x/x)*f(1+i*step,1+j*step,x+1,y+1));
    }
}

void inline nextGridInterpolation(double* source, double* dest, int alt, int step, int x, int y){
    for(int j = y/step;j > 0;j--){
        for(int i = x/step;i > 0;i--){
            dest[((int)pow(2,alt+1)+2)*2*i+2*j] = source[((int)pow(2,alt)+2)*i+j];
            dest[((int)pow(2,alt+1)+2)*2*i+2*j-1] = source[((int)pow(2,alt)+2)*i+j];
            dest[((int)pow(2,alt+1)+2)*(2*i-1)+2*j] = source[((int)pow(2,alt)+2)*i+j];
            dest[((int)pow(2,alt+1)+2)*(2*i-1)+2*j-1] = source[((int)pow(2,alt)+2)*i+j];
        }
    }
}

//сложение и интерполяция объединены
void inline nextGridSum(double* sum, double* dest, int alt, int step, int x, int y){
    for(int j = y/step;j > 0;j--){
        for(int i = x/step;i > 0;i--){
            dest[((int)pow(2,alt+1)+2)*2*i+2*j] += sum[((int)pow(2,alt)+2)*i+j];
            dest[((int)pow(2,alt+1)+2)*2*i+2*j-1] += sum[((int)pow(2,alt)+2)*i+j];
            dest[((int)pow(2,alt+1)+2)*(2*i-1)+2*j] += sum[((int)pow(2,alt)+2)*i+j];
            dest[((int)pow(2,alt+1)+2)*(2*i-1)+2*j-1] += sum[((int)pow(2,alt)+2)*i+j];
        }
    }
}
//все верно
void inline residualCompute(double* source, double* residual, int alt, int step, int x, int y){
    for(int j = 1;j < y/step+1;j++)
        for(int i = 1;i < x/step+1;i++)
            residual[((int)pow(2,alt)+2)*i+j] = x/step*y/step*(source[((int)pow(2,alt+1)+2)*(i+1)+j]+source[((int)pow(2,alt+1)+2)*(i-1)+j]
                    +source[((int)pow(2,alt+1)+2)*i+j+1]+source[((int)pow(2,alt+1)+2)*i+j-1]-4*source[((int)pow(2,alt+1)+2)*i+j]) + f(i,j,x/step+2,x/step+2);
}

void inline jacobyMethod(double* source, double* dest, double* f, int alt, int step, int x, int y){
    for(int j = 1;j < y/step/2+2-1;j++)
        for(int i = 1;i < x/step/2+2-1;i++)
            dest[((int)pow(2,alt)+2)*i+j] = 0.25*(source[((int)pow(2,alt)+2)*(i-1)+j]+source[((int)pow(2,alt)+2)*(i+1)+j]
                    +source[((int)pow(2,alt)+2)*i+j-1]+source[((int)pow(2,alt)+2)*i+j+1]+((double)step/2*step/2/x/x)*f[((int)pow(2,alt)+2)*i+j]);
}

void inline jacobyMethodF(double* source, double* dest, int alt, int step, int x, int y){
    //здесь все верно
    for(int j = 1;j < y/step+1;j++)
        for(int i = 1;i < x/step+1;i++)
            dest[((int)pow(2,alt)+2)*i+j]=0.25*(source[((int)pow(2,alt)+2)*(i-1)+j]+source[((int)pow(2,alt)+2)*(i+1)+j]
                    +source[((int)pow(2,alt)+2)*i+j-1]+source[((int)pow(2,alt)+2)*i+j+1]+(step*step/x/x)*f(i,j,x/step+2,x/step+2));
}

void inline residualRestriction(double* residual, int alt, int step, int x, int y){
    //КАЖЕТСЯ, все верно
    //ВНИМАНИЕ!!! теперь все данные находятся в верхнем левом углу
    for(int j = 0;j < y/step+1;j+=2)
        for(int i = 0;i < y/step+1;i+=2)
            residual[((int)pow(2,alt)+2)*(1+i/2)+1+j/2] = (residual[((int)pow(2,alt)+2)*i+j]+residual[((int)pow(2,alt)+2)*(i+1)+j]
                    +residual[((int)pow(2,alt)+2)*i+j+1]+residual[((int)pow(2,alt)+2)*(i+1)+j+1])/4;

    //поправим испорченные граничные условия справа и снизу
    for(int i = 0;i < y/step/2+1;i++){
        residual[((int)pow(2,alt)+2)*(1+i)+x/step/2+1] = 0;
        residual[((int)pow(2,alt)+2)*(x/step/2+1)+1+i] = 0;
    }
}

Window::Window():
    dlg(new Dialog()){

    dlg->exec();

    x = dlg->x;
    y = dlg->y;
    t = dlg->t;
    alpha = dlg->alpha;

    //delete dlg;

    double **residual;
    double **correction;

    data = new double**[x+2];
    residual = new double*[(int)log2(x)];
    correction = new double*[(int)log2(x)];

    for (int n = 1; n <= (int)log2(x); n++){
        correction[n] = new double[((int)pow(2,n)+2)*((int)pow(2,n)+2)];
        for (int i = 0; i < (int)pow(2,n)+2; i++)
            for (int j = 0; j < (int)pow(2,n)+2; j++)
                correction[n][i*((int)pow(2,n)+2) + j] = 0;
    }

    for (int n = 0; n <= (int)log2(x); n++){
        residual[n] = new double[2*2*((int)pow(2,n)+2)*((int)pow(2,n)+2)];
        for (int i = 0; i < 2*((int)pow(2,n)+2); i++)
            for (int j = 0; j < 2*((int)pow(2,n)+2); j++)
                residual[n][i*((int)pow(2,n)+2) + j] = 0;
    }

    for (int i = 0; i <= x+2; i++){
        data[i] = new double*[y+2];

        for (int j = 0; j <= y+2; j++){
            data[i][j] = new double[t];
            for (int n = 0; n<t;n++)
                data[i][j][n] = 0;
        }
    }

    //prepairing finished

    //first iterations on the coastest grid
    //тут все верно
    int step = x/4;

    coastestGridIteration(correction, step, x, y);

    //main loop for the V-cycles
    for(int n = 3, alt = 2; step != 1; alt++ ){

        nextGridInterpolation(correction[alt], correction[alt+1], alt, step, x, y);

        step/= 2;//соотвествует alt+1
        qDebug() << step;
        //находимся в стартовой точке v-цикла
        applyBounds(correction, alt+1, step, x, y);
        applyZeroBounds(correction, alt, 2*step, x, y);

        //pre-smoothing
        for(int check = n;n < check+3; n++){
            jacobyMethodF(correction[alt+1], correction[alt+1], alt+1, step, x ,y);
        }


        //moving down
        for (int internalStep = step; internalStep != x/4; internalStep*=2, alt--){
            residualCompute(correction[alt+1],residual[alt],alt, internalStep, x, y);

            residualRestriction(residual[alt], alt, internalStep, x, y);

            clearLayer(correction, alt, 2*internalStep,x,y);
            //Improve grid correction
            jacobyMethod(correction[alt], correction[alt], residual[alt], alt, internalStep, x, y);
        }

        alt++;

        //moving up
        for (int internalStep = x/4; internalStep != 2*step; internalStep/=2, alt++){
            //u+ interpolated v
            nextGridSum(correction[alt], correction[alt+1], alt, internalStep, x, y);

            //post-smoothing
            for(int check = n;n < check+3; check--){
                jacobyMethod(correction[alt+1], correction[alt+1], residual[alt+1], alt+1, internalStep/2, x, y);
            }
        }

        //u + interpolated v
        nextGridSum(correction[alt], correction[alt+1], alt, 2*step, x, y);

        applyBounds(correction, alt+1, step, x, y);
        //post-smoothing
        for(int check = n;n < check+3; check--){
            jacobyMethodF(correction[alt+1], correction[alt+1], alt+1, step, x, y);
        }

        if( step == 1){
            applyDataBounds(data, 0, 1, x, y);

            //считаем
            for(int j =1;j < y+1;j++)
                for(int i = 1;i < x+1;i++){
                    data[i][j][0]= correction[alt+1][((int)pow(2,alt+1)+2)*i+j];

                }
        }
    }


    glWidget = new GLWidget(data, x, y, t);

    timeSlider = createSlider();

    connect(timeSlider, SIGNAL(valueChanged(int)), glWidget, SLOT(setTime(int)));
    connect(glWidget, SIGNAL(timeChanged(int)), timeSlider, SLOT(setValue(int)));

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(glWidget);
    mainLayout->addWidget(timeSlider);
    setLayout(mainLayout);
    timeSlider->setMaximum(t);
    timeSlider->setValue(0);
    setWindowTitle(tr("Hello GL"));
}

QSlider *Window::createSlider(){
    QSlider *slider = new QSlider(Qt::Vertical);
    slider->setRange(0, 100);
    slider->setSingleStep(10);
    slider->setPageStep(50);
    slider->setTickInterval(50);
    slider->setTickPosition(QSlider::TicksRight);
    return slider;
}

void Window::keyPressEvent(QKeyEvent *e){
    if (e->key() == Qt::Key_Escape)
        close();
    else
        QWidget::keyPressEvent(e);
}

Window::~Window(){
   // delete glWidget;
    /*for (int i = 0; i < x; i++){
        for (int j = 0; j < y; j++)
            delete[] data[i][j];
    }
*/
    /*for (int i = 0; i < x+2; i++)
        delete[] data[i];

    delete[] data;*/
}

/*
    //pre-smoothing
    for(int n = 0;n < 10; n++){
        for(int j = 1;j < y-1;j++)
            for(int i = 1;i < x-1;i++)
                data[i][j][n+1]=0.25*(data[i-1][j][n]+data[i+1][j][n]+data[i][j-1][n]+data[i][j+1][n]+(1.0/x/x)*f(i,j,x,y));
    }

    //Computing the residual
    for(int j = 1;j < y-1;j++)
        for(int i = 1;i < x-1;i++)
            residual[i][j] = x*y*(data[i+1][j][10]+data[i-1][j][10]+data[i][j+1][10]+data[i][j-1][10]-4*data[i][j][10]) +f(i,j,x,y);

    //Restrict residual to coaster grid
    for(int j = 1;j < y-1;j+=2)
        for(int i = 1;i < x-1;i+=2)
            residual[i][j] = (residual[i][j]+residual[i+1][j]+residual[i][j+1]+residual[i+1][j+1])/4;

    //Improve grid correction
    for(int n = 0;n < 10; n++)
        for(int j = 3;j < y-3;j+=2)
            for(int i = 3;i < x-3;i+=2)
                correction[i][j] = 0.25*(correction[i-2][j]+correction[i+2][j]+correction[i][j-2]+correction[i][j+2]+(1.0/x/x)*residual[i][j]);

    //Interpolation on the better grid
    for(int j = 1;j < y-1;j+=2)
        for(int i = 1;i < x-1;i+=2){
            correction[i+1][j] = correction[i][j];
            correction[i][j+1] = correction[i][j];
            correction[i+1][j+1] = correction[i][j];
        }
    //u+v
    for(int j = 1;j < y-1;j++)
        for(int i = 1;i < x-1;i++)
            data[i][j][10] += correction[i][j];

    //post-smoothing
    for(int n = 10;n < 20; n++){
        for(int j = 1;j < y-1;j++)
            for(int i = 1;i < x-1;i++)
                data[i][j][n+1]=0.25*(data[i-1][j][n]+data[i+1][j][n]+data[i][j-1][n]+data[i][j+1][n])+(1.0/x)*0.25*f(i,j,x,y);
    }
*/
