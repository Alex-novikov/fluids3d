
#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include <QWindow>
#include <QTime>

class AbstractScene;

class QOpenGLContext;

class GLWidget : public QGLWidget
{
    Q_OBJECT

public:
    GLWidget(double***data, int x, int y, int t, QWidget *parent = 0);
    ~GLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

public slots:
    void setTime(int time);
protected slots:
        void updateScene();

signals:
    void timeChanged(int time);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private:
    int time;
    QPoint lastPos;
    QColor qtGreen;
    QColor qtPurple;
    AbstractScene* m_scene;
    bool m_leftButtonPressed;
    QPoint m_prevPos;
    QPoint m_pos;
    QTime m_time;

};

#endif
