
#include "Solver/All.h"
#include "Solver\goldKernels\goldJacobyKernel.h"
#include "Solver\Helper.h"

bool selector(int value){
	if (value == (int)CellType::Boundary)
		return true;
	return false;
}
int main(int argc, char *argv[])
{
	try
	{
		printf("Temporary project for testing only. Will be removed after the \"Benchmark\" project creation.");

		size_t NX = 130, NY = 130, NZ = 130;
		PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);
	
		std::vector<std::pair<DeviceDescriptor, float> > devices;
		devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
		solver->Configure(devices);

		Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::MappedHost, NX, NY, NZ);
		Grid<real> *direchletValues = Grid<real>::Create(MemoryType::MappedHost, NX, NY, NZ);
		Grid<real> *neumannValues = Grid<real>::Create(MemoryType::MappedHost, NX, NY, NZ);
		neumannValues->Clear();
		Grid<real> *neumannXValues = Grid<real>::Create(MemoryType::MappedHost, NX, NY, NZ);
		Grid<real> *neumannYValues = Grid<real>::Create(MemoryType::MappedHost, NX, NY, NZ);
		Grid<real> *neumannZValues = Grid<real>::Create(MemoryType::MappedHost, NX, NY, NZ);
		Grid<int> *mask = Grid<int>::Create(MemoryType::MappedHost, NX, NY, NZ);

		for (size_t z = 0; z < NZ; z++)
			for (size_t y = 0; y < NY; y++)
				for (size_t x = 0; x < NX; x++)
				{
					bool isDirichlet = (x == 0) || (x == NX - 1) ||
						(y == 0) || (y == NY - 1) ||
						(z == 0) || (z == NZ - 1);

					int index = boundaryMask->ComputeIndex(x, y, z);

					real rx = (real)x / (NX - 1) - 0.5;
					real ry = (real)y / (NY - 1) - 0.5;
					real rz = (real)z / (NZ - 1) - 0.5;

					real r2 = rx*rx + ry*ry + rz*rz;
					real l = sqrt(r2);

					if (isDirichlet){
						boundaryMask->Pointer()[index] = (int)BoundaryCondition::Dirichlet;
						direchletValues->Pointer()[index] = 1000 * 0.25*0.25*0.25 / 2 / r2*rz / l;
						mask->Pointer()[index] = (int)CellType::Boundary;
					}
					else {
						//r=0.25
						if ((rx*rx) + (ry*ry) + (rz*rz) < 0.0625)
						{

							mask->Pointer()[index] = (int)CellType::Dead;
						}
						else {
							mask->Pointer()[index] = (int)CellType::Working;
						}

						boundaryMask->Pointer()[index] = 0;
						direchletValues->Pointer()[index] = 0.0f;
					}
				}

		int* bMPtr = boundaryMask->Pointer();
		int* mPtr = mask->Pointer();

		for (size_t z = 0; z < NZ; z++)
			for (size_t y = 0; y < NY; y++)
				for (size_t x = 0; x < NX; x++)
				{
					int index = boundaryMask->ComputeIndex(x, y, z);

					if (mPtr[index] == (int)CellType::Working){

						int pitchY = boundaryMask->PitchY();
						int pitchZ = boundaryMask->PitchZ();

						if (mPtr[index + 1] == (int)CellType::Dead
							|| mPtr[index - 1] == (int)CellType::Dead
							|| mPtr[index + pitchY] == (int)CellType::Dead
							|| mPtr[index - pitchY] == (int)CellType::Dead
							|| mPtr[index + pitchZ] == (int)CellType::Dead
							|| mPtr[index - pitchZ] == (int)CellType::Dead)
						{
							//����� � ������ [-0.5, 0.5]
							real rx = (real)x / (NX - 1) - 0.5;
							real ry = (real)y / (NY - 1) - 0.5;
							real rz = (real)z / (NZ - 1) - 0.5;
							real l = sqrt(rx*rx + ry*ry + rz*rz);

							bMPtr[index] = (int)BoundaryCondition::Neumann;
							//���� ��
							neumannValues->Pointer()[index] = -1000 * rz / l;
							neumannXValues->Pointer()[index] = rx / l;
							neumannYValues->Pointer()[index] = ry / l;
							neumannZValues->Pointer()[index] = rz / l;
							mPtr[index] = (int)CellType::Boundary;
						}
					}
				}

		real* nptr = neumannValues->Pointer();
		Grid<real> *res = Grid<real>::Create(MemoryType::MappedHost, NX, NY, NZ);
		Grid<real> *f = Grid<real>::Create(MemoryType::MappedHost, NX, NY, NZ);
		f->Clear();
		CompressedIndices *boundary = CompressedIndices::Create(mask, &selector, MemoryType::Device);
		CompressedIndices *boundaryCPU = CompressedIndices::Create(mask, &selector, MemoryType::Host);
		CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundaryCPU, MemoryType::Device);
		delete boundaryMask;
		CompressedValues<real> *dValues = CompressedValues<real>::Create(direchletValues, boundaryCPU, MemoryType::Device);
		delete direchletValues;
		CompressedValues<real> *nValues = CompressedValues<real>::Create(neumannValues, boundaryCPU, MemoryType::Device);
		delete neumannValues;
		CompressedValues<real> *nxValues = CompressedValues<real>::Create(neumannXValues, boundaryCPU, MemoryType::Device);
		delete neumannXValues;
		CompressedValues<real> *nyValues = CompressedValues<real>::Create(neumannYValues, boundaryCPU, MemoryType::Device);
		delete neumannYValues;
		CompressedValues<real> *nzValues = CompressedValues<real>::Create(neumannZValues, boundaryCPU, MemoryType::Device);
		delete neumannZValues;
		delete boundaryCPU;

		PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, dValues, nValues, nxValues, nyValues, nzValues, 1.0 / (NX - 1), 1e-3f, 10000000, DeviceType::GPU);
		
		delete f;
		delete boundary;
		delete boundaryType;
		delete dValues;
		delete nValues;
		delete nxValues;
		delete nyValues;
		delete nzValues;

		Grid<real> *err = Grid<real>::Create(MemoryType::MappedHost, NX, NY, NZ);

		printf("Performance information:\n");
		for (size_t i = 0; i < pInfo.Records().size(); i++)
		{
			const PerfRecord &rec = pInfo.Records()[i];

			printf("   %s: %lf seconds\n", rec.Description().c_str(), rec.ElapsedTime());
			printf("      %Performance: %lf %s\n", rec.PerformanceValue(), rec.PerformanceUnits().c_str());
			printf("      %Bandwidth:   %lf %s\n", rec.BandwidthValue(), rec.BandwidthUnits().c_str());
			printf("\n");
		}

		real max = 0;
		for (size_t z = 0; z < NZ; z++)
			for (size_t y = 0; y < NY; y++)
				for (size_t x = 0; x < NX; x++)
				{
					int index = res->ComputeIndex(x, y, z);
					if (mask->Pointer()[index] == CellType::Working){
						real rx = (real)x / (NX - 1) - 0.5;
						real ry = (real)y / (NY - 1) - 0.5;
						real rz = (real)z / (NZ - 1) - 0.5;

						real r2 = rx*rx + ry*ry + rz*rz;
						real l = sqrt(r2);

						real tm = std::abs(res->Pointer()[index] - 1000 * 0.25*0.25*0.25 / 2 / r2*rz / l);
						err->Pointer()[index] = tm;
						if (tm > max)
							max = tm;
					}
				}
		printf("The residual is:%f", max);

		Helper::GridSave(res, "result.csv");
		Helper::GridSave(err, "err.csv");

		delete res;
		delete err;
		delete mask;

		delete solver;
		getchar();
		getchar();
		getchar();
		return 0;
	}
	catch (std::exception &ex)
	{
		printf("The following error has occured: %s\n", ex.what());
		return 42;
	}
}
