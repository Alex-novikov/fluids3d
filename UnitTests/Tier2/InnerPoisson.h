
#pragma once

using namespace Microsoft::VisualStudio::TestTools::UnitTesting;

#include "Solver/All.h"

#include <map>

namespace UnitTests
{
	bool selector(int value){
		if (value == (int)BoundaryCondition::Dirichlet)
			return true;
		return false;
	}

	[TestClass]
	public ref class InnerPoisson
	{
	
	public:
		[TestMethod]
		void ZeroBounds()
		{
			size_t NX = 3, NY = 3, NZ = 3;
			PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);

			std::vector<std::pair<DeviceDescriptor, float> > devices;
			devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
			solver->Configure(devices);

			Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *boundaryValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++)
					{
						bool isBoundary = (x == 0) || (x == NX - 1) ||
							(y == 0) || (y == NY - 1) ||
							(z == 0) || (z == NZ - 1);

						int index = boundaryMask->ComputeIndex(x, y, z);
						boundaryMask->Pointer()[index] = isBoundary ? (int)BoundaryCondition::Dirichlet : 0;
						boundaryValues->Pointer()[index] = 0;
						mask->Pointer()[index] = isBoundary ? (int)CellType::Boundary : (int)CellType::Working;
					}

			Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			f->Clear();

			CompressedIndices *boundary = CompressedIndices::Create(boundaryMask, &selector);
			CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
			CompressedValues<real> *values = CompressedValues<real>::Create(boundaryValues, boundary);

			PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, values, values, values, values, values, 1.0 / NX, 1e-5f);

			for (int i = 0; i < NX*NY*NZ; i++){
				if (abs(res->Pointer()[i]) > 1.0 / NX)
					throw gcnew System::ApplicationException("Bad solver");
			}
		}

		[TestMethod]
		void DirichletProblem()
		{
			size_t NX = 6, NY = 6, NZ = 6;
			PoissonSolver *solver = PoissonSolver::Create(NX, NY, NZ);

			std::vector<std::pair<DeviceDescriptor, float> > devices;
			devices.push_back(std::make_pair(DeviceDescriptor::CPU(1), 1.0f));
			solver->Configure(devices);

			Grid<int> *boundaryMask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *boundaryValues = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<int> *mask = Grid<int>::Create(MemoryType::Host, NX, NY, NZ);
			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++)
					{
						bool isBoundary = (x == 0) || (x == NX - 1) ||
							(y == 0) || (y == NY - 1) ||
							(z == 0) || (z == NZ - 1);

						int index = boundaryMask->ComputeIndex(x, y, z);
						boundaryMask->Pointer()[index] = isBoundary ? (int)BoundaryCondition::Dirichlet : 0;
						boundaryValues->Pointer()[index] = isBoundary ? std::abs((float)x / (NX-1) + (float)y / (NY-1) + (float)z / (NZ-1)) : 0.0f;
						mask->Pointer()[index] = isBoundary ? (int)CellType::Boundary : (int)CellType::Working;
					}

			Grid<real> *res = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			Grid<real> *f = Grid<real>::Create(MemoryType::Host, NX, NY, NZ);
			f->Clear();

			CompressedIndices *boundary = CompressedIndices::Create(boundaryMask, &selector);
			CompressedValues<int> *boundaryType = CompressedValues<int>::Create(boundaryMask, boundary);
			CompressedValues<real> *values = CompressedValues<real>::Create(boundaryValues, boundary);

			PerfInfo pInfo = solver->Solve(res, f, mask, boundary, boundaryType, values, values, values, values, values, 1.0 / NX, 1e-5f);
			
			real* ptr = res->Pointer();
			for (size_t z = 0; z < NZ; z++)
				for (size_t y = 0; y < NY; y++)
					for (size_t x = 0; x < NX; x++){
						int index = res->ComputeIndex(x, y, z);
						if (std::abs(res->Pointer()[index] - float(x) / (NX-1) - float(y) / (NY-1) - float(z) / (NZ-1)) > 3.0 / NX){
							System::Console::WriteLine(x);
							System::Console::WriteLine(y);
							System::Console::WriteLine(z);
							System::Console::WriteLine(res->Pointer()[index]);
							throw gcnew System::ApplicationException("Bad solver");
						}
			}
		}
	};
}