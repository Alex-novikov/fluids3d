
#pragma once

using namespace Microsoft::VisualStudio::TestTools::UnitTesting;

#include "Solver/All.h"

#include <map>

namespace UnitTests
{
	static inline bool IsNotZero(int val)
	{ return val != 0; }

	[TestClass]
	public ref class CompressedMatrices
	{
		private:
			static void UniTest(Grid<int> *matrix)
			{
				//Step 1. Generating compressed indices for nonzero elements.
				std::auto_ptr<CompressedIndices> _cIndices(CompressedIndices::Create(matrix, IsNotZero));
				CompressedIndices *cIndices = _cIndices.get();

				//Step 2. Generating list with nonzero values.
				std::auto_ptr<CompressedValues<int> > _cValues(CompressedValues<int>::Create(matrix, cIndices));
				CompressedValues<int> *cValues = _cValues.get();

				//Step 3. Checking correctness (the same amount of the same elements).
				std::map<int, size_t> dCounts;
				size_t dNonzero = 0;
				for (size_t z = 0; z < matrix->SizeZ(); z++)
					for (size_t y = 0; y < matrix->SizeY(); y++)
						for (size_t x = 0; x < matrix->SizeX(); x++)
						{
							size_t index = matrix->ComputeIndex(x, y, z);
							int val = matrix->Pointer()[index];
							if (val != 0)
							{
								dCounts[val] += 1;
								dNonzero += 1;
							}
						}

				std::map<int, size_t> cCounts;
				for (size_t i = 0; i < cValues->Count(); i++)
				{
					cCounts[cValues->Pointer()[i]] += 1;
				}

				if (cValues->Count() != cIndices->Count())
				{ throw gcnew System::ApplicationException("Number of the compressed indices and compressed values are not the same"); } 

				if (cValues->Count() != dNonzero)
				{ throw gcnew System::ApplicationException("Dense matrix has another number of the nonzero elements"); }

				for (std::map<int, size_t>::iterator it = dCounts.begin();
					it != dCounts.end();
					it++)
				{
					if (cCounts[it->first] != it->second)
						throw gcnew System::ApplicationException("Some values from dense matrix were lost or duplicated");
				}

				//Step 4. Checking correctness. Again (the same indices).
				for (size_t z = 0; z < cIndices->SizeZ(); z++)
				{
					for (int yID = cIndices->ZOffsets()[z]; yID < cIndices->ZOffsets()[z + 1]; yID++)
					{
						size_t y = cIndices->YIndices()[yID];
						for (int xID = cIndices->YOffsets()[yID]; xID < cIndices->YOffsets()[yID + 1]; xID++)
						{
							size_t x = cIndices->XIndices()[xID];
							
							int cValue = cValues->Pointer()[xID];
							size_t dOffset = matrix->ComputeIndex(x, y, z);
							if (cValue != matrix->Pointer()[dOffset])
							{ throw gcnew System::ApplicationException("Wrong indices in the structure with compressed indices"); }
						}
					}
				}

			}

			static void Creation_Rand(size_t sizeX, size_t sizeY, size_t sizeZ)
			{
				std::auto_ptr<Grid<int> > matrix(Grid<int>::Create(MemoryType::Host, sizeX, sizeY, sizeZ));

				for (size_t z = 0; z < matrix->SizeZ(); z++)
					for (size_t y = 0; y < matrix->SizeY(); y++)
						for (size_t x = 0; x < matrix->SizeX(); x++)
						{
							size_t index = matrix->ComputeIndex(x, y, z);

							int r = rand() & 0xFF;

							matrix->Pointer()[index] = (r & 1) == 0 ? 0 : (r >> 1);
						}

				UniTest(matrix.get());
			}

			static void Enumeration_Rand(size_t sizeX, size_t sizeY, size_t sizeZ)
			{
				//Creating grid with randomly selected elements.
				std::auto_ptr<Grid<int> > matrix(Grid<int>::Create(MemoryType::Host, sizeX, sizeY, sizeZ));
				std::map<unsigned long long int, bool> values;

				for (size_t z = 0; z < matrix->SizeZ(); z++)
					for (size_t y = 0; y < matrix->SizeY(); y++)
						for (size_t x = 0; x < matrix->SizeX(); x++)
						{
							size_t index = matrix->ComputeIndex(x, y, z);
							
							if (rand() & 1)
							{
								values[x * 1000 * 1000 + y * 1000 + z] = true;
								matrix->Pointer()[index] = 1;
							}
							else
								matrix->Pointer()[index] = 0;
						}

				//Creating indices and checking their enumerator.
				std::auto_ptr<CompressedIndices> indices(CompressedIndices::Create(matrix.get(), IsNotZero));
				
				CompressedIndices::Enumerator en = indices->GetEnumerator();
				size_t count = 0;
				while (en.MoveNext())
				{
					if (values.find(en.X() * 1000 * 1000 + en.Y() * 1000 + en.Z()) == values.end())
						throw gcnew System::ApplicationException("Some elements were added!");
					count += 1;
				}
				
				if (count != indices->Count())
					throw gcnew System::ApplicationException("Some elements were lost");
			}

		public:
			[TestMethod]
			void CompressedMatrices_Creation_Rand_3x3x3()
			{ Creation_Rand(3, 3, 3); }

			[TestMethod]
			void CompressedMatrices_Creation_Rand_8x8x8()
			{ Creation_Rand(8, 8, 8); }

			[TestMethod]
			void CompressedMatrices_Creation_Rand_10x2x5()
			{ Creation_Rand(10, 2, 5); }

			[TestMethod]
			void CompressedMatrices_Creation_Rand_1x16x1()
			{ Creation_Rand(1, 16, 1); }

			[TestMethod]
			void CompressedMatrices_Enumeration_Rand_16x16x16()
			{ Enumeration_Rand(16, 16, 16); }

			[TestMethod]
			void CompressedMatrices_Enumeration_Rand_16x4x8()
			{ Enumeration_Rand(16, 4, 8); }

			[TestMethod]
			void CompressedMatrices_Enumeration_Rand_1x4x1()
			{ Enumeration_Rand(1, 4, 1); }
	};
}
