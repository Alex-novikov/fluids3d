
#pragma once

using namespace Microsoft::VisualStudio::TestTools::UnitTesting;

#include "Solver/All.h"

#include <map>

namespace UnitTests
{
	[TestClass]
	public ref class Grids
	{
		public:
			static void Enumeration_Rand(int sizeX, int sizeY, int sizeZ)
			{
				std::unique_ptr<Grid<int> > _grid(Grid<int>::Create(MemoryType::Host, sizeX, sizeY, sizeZ));

				//Initializing with random values.
				for (size_t z = 0; z < _grid->SizeZ(); z++)
					for (size_t y = 0; y < _grid->SizeY(); y++)
						for (size_t x = 0; x < _grid->SizeX(); x++)
						{
							_grid->Pointer()[_grid->ComputeIndex(x, y, z)] = (rand() % 42) + 1;
						}

				//Enumerating and checkig.
				size_t foundElements = 0;
				auto en = _grid->GetEnumerator();
				while (en.MoveNext())
				{
					if (en.Value() != _grid->Pointer()[_grid->ComputeIndex(en.X(), en.Y(), en.Z())] || en.Value() <= 0)
						System::ApplicationException("Grid::Enumerator have broked indices, it's not good ...");
					_grid->Pointer()[_grid->ComputeIndex(en.X(), en.Y(), en.Z())] = -1;
					foundElements += 1;
				}

				//Checking total count of elements.
				if (foundElements != _grid->SizeX() * _grid->SizeY() * _grid->SizeZ())
				{ throw gcnew System::ApplicationException("Some Grid's elements were missed (or added) by enumerator"); }
			}

		[TestMethod]
		void Grids_Enumeration_Rand_1x1x1()
		{ Enumeration_Rand(1, 1, 1); }

		[TestMethod]
		void Grids_Enumeration_Rand_4x2x1()
		{ Enumeration_Rand(4, 2, 1); }

		[TestMethod]
		void Grids_Enumeration_Rand_5x7x3()
		{ Enumeration_Rand(5, 7, 3); }

		[TestMethod]
		void Grids_Enumeration_Rand_16x16x16()
		{ Enumeration_Rand(16, 16, 16); }
	};
}
