#pragma once

#include "Common\Grid.h"
#include <iostream>
#include <fstream>

using namespace std;

class Helper
{
public:
	static void cudaCheckError(cudaError err);

	template< class T >
	static void GridSave(Grid<T>* grid, const char* filename = "GridSave.csv"){
		ofstream f;
		f.open(filename);


		std::cout << "error: " << strerror(errno) << std::endl;

		f /*<< "X" << "," << "Y" << "," << "Z" << "," */ << "Data";

		for (size_t z = 0; z < grid->SizeZ() - 1; z++)
			f << ",";
		f << endl;

		for (size_t y = 0; y < grid->SizeY(); y++){
			/*f << y << "," << y << "," << y;*/
			for (size_t x = 0; x < grid->SizeX(); x++){
				int index = grid->ComputeIndex(x, y, 0);
				if (x == 0)
					f << (T)grid->Pointer()[index];
				else
					f << "," << (T)grid->Pointer()[index];
			}
			f << endl;
		}
		for (size_t z = 1; z < grid->SizeZ(); z++)
		for (size_t y = 0; y < grid->SizeY(); y++){
			//f << ",,";
			for (size_t x = 0; x < grid->SizeX(); x++){
				int index = grid->ComputeIndex(x, y, z);
				if (x == 0)
					f << (T)grid->Pointer()[index];
				else
					f << "," << (T)grid->Pointer()[index];
			}
			f << endl;
		}
		f << endl;
		f << "ID,Column,Variable Name,Data Type,Rank,Missing Value,Dimensions" << endl;
		f << "1,A:DZ,Data,Double,3,,_1:" << grid->SizeX() << " _2:" << grid->SizeY() << " _3:" << grid->SizeZ();
		f.close();
	};


	/*template< class T > 
	static void SelectiveRestriction(Grid<T>* dstLayer, Grid<T>* srcLayer, Region dstRegion, Region srcRegion){
		T *dst = dstLayer->Pointer();
		T *src = srcLayer->Pointer();
		size_t ratioX = (srcLayer->SizeX()-2) / (dstLayer->SizeX()-2);
		size_t ratioY = (srcLayer->SizeY()-2) / (dstLayer->SizeY()-2);
		size_t ratioZ = (srcLayer->SizeZ()-2) / (dstLayer->SizeZ()-2);

		if (!(ratioX && ratioY && ratioZ))
			throw std::runtime_error("The destination grid is finer then the source grid while restriction.");


		//� ��������� ��� ����� ���, ����� �� ����� ���� ������ �������������� if �� �������
		//�������� ����������� CUDA ��� ��� ����� � ��� ������ �� �����
		size_t srcIndex, dstIndex;
		for (size_t z = dstRegion.OffsetZ(); z < dstRegion.OffsetZ() + dstRegion.SizeZ() - 1; z++){
			for (size_t y = dstRegion.OffsetY(); y < dstRegion.OffsetY() + dstRegion.SizeY() - 1; y++){
				for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
				{
					srcIndex = srcLayer->ComputeIndex(x * ratioX, y * ratioY, z * ratioZ);
					dstIndex = dstLayer->ComputeIndex(x, y, z);
					dst[dstIndex] = src[srcIndex];
				}
				//on X bound {
				srcIndex = srcLayer->ComputeIndex(srcRegion.OffsetX() + srcRegion.SizeX() - 1, y * ratioY, z * ratioZ);
				dstIndex = dstLayer->ComputeIndex(dstRegion.OffsetX() + dstRegion.SizeX() - 1, y, z);
				dst[dstIndex] = src[srcIndex];
				//}
			}
			//on Y bound {
			for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
			{
				srcIndex = srcLayer->ComputeIndex(x * ratioX, srcRegion.OffsetY() + srcRegion.SizeY() - 1, z * ratioZ);
				dstIndex = dstLayer->ComputeIndex(x, dstRegion.OffsetY() + dstRegion.SizeY() - 1, z);
				dst[dstIndex] = src[srcIndex];
			}
			srcIndex = srcLayer->ComputeIndex(srcRegion.OffsetX() + srcRegion.SizeX() - 1, srcRegion.OffsetY() + srcRegion.SizeY() - 1, z * ratioZ);
			dstIndex = dstLayer->ComputeIndex(dstRegion.OffsetX() + dstRegion.SizeX() - 1, dstRegion.OffsetY() + dstRegion.SizeY() - 1, z);
			dst[dstIndex] = src[srcIndex];
			//}
		}

		//on Z bound {
		for (size_t y = dstRegion.OffsetY(); y < dstRegion.OffsetY() + dstRegion.SizeY() - 1; y++){
			for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
			{
				srcIndex = srcLayer->ComputeIndex(x * ratioX, y * ratioY, srcRegion.OffsetZ() + srcRegion.SizeZ() - 1);
				dstIndex = dstLayer->ComputeIndex(x, y, dstRegion.OffsetZ() + dstRegion.SizeZ() - 1);
				dst[dstIndex] = src[srcIndex];
			}
			//on X bound {
			srcIndex = srcLayer->ComputeIndex(srcRegion.OffsetX() + srcRegion.SizeX() - 1, y * ratioY, srcRegion.OffsetZ() + srcRegion.SizeZ() - 1);
			dstIndex = dstLayer->ComputeIndex(dstRegion.OffsetX() + dstRegion.SizeX() - 1, y, dstRegion.OffsetZ() + dstRegion.SizeZ() - 1);
			dst[dstIndex] = src[srcIndex];
			//}
		}
		//on Y bound{
		for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
		{
			srcIndex = srcLayer->ComputeIndex(x * ratioX,
				srcRegion.OffsetY() + srcRegion.SizeY() - 1,
				srcRegion.OffsetZ() + srcRegion.SizeZ() - 1);
			dstIndex = dstLayer->ComputeIndex(x,
				dstRegion.OffsetY() + dstRegion.SizeY() - 1,
				dstRegion.OffsetZ() + dstRegion.SizeZ() - 1);
			dst[dstIndex] = src[srcIndex];
		}
		//}
		srcIndex = srcLayer->ComputeIndex(srcRegion.OffsetX() + srcRegion.SizeX() - 1,
			srcRegion.OffsetY() + srcRegion.SizeY() - 1,
			srcRegion.OffsetZ() + srcRegion.SizeZ() - 1);
		dstIndex = dstLayer->ComputeIndex(dstRegion.OffsetX() + dstRegion.SizeX() - 1,
			dstRegion.OffsetY() + dstRegion.SizeY() - 1,
			dstRegion.OffsetZ() + dstRegion.SizeZ() - 1);
		dst[dstIndex] = src[srcIndex];
		//}
	}*/
	~Helper();
};

