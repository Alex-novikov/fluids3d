
#pragma once

#include "Definitions.h"

class IMemoryBlock
{
	private:
		void *_ptr;
		MemoryType::Type _memType;
		size_t _sizeInBytes;

	private:
		inline IMemoryBlock(const IMemoryBlock &)
		{ throw std::runtime_error("Ata-ta"); }

		inline void operator =(const IMemoryBlock &)
		{ throw std::runtime_error("Ata-ta"); }

	protected:
		inline IMemoryBlock(void *ptr, MemoryType::Type memType, size_t sizeInBytes)
			: _ptr(ptr), _memType(memType), _sizeInBytes(sizeInBytes)
		{ /*nothing*/ }

	public:
		inline void *Pointer() const
		{  return _ptr; }

		inline MemoryType::Type MemoryType() const
		{ return _memType; }

		inline size_t Size() const
		{ return _sizeInBytes; }

		virtual void Clear() = 0;

		virtual ~IMemoryBlock()
		{ /*nothing*/ };
};

//Allocates and releases memory blocks.
class MemoryFactory
{
	private:
		inline MemoryFactory()
		{ throw std::runtime_error("Ata-ta"); }

		inline MemoryFactory(const MemoryFactory &)
		{ throw std::runtime_error("Ata-ta"); }

	public:
		static IMemoryBlock *Allocate(MemoryType::Type memType, size_t sizeInBytes);

		static void Copy(IMemoryBlock *dst, size_t dstOffsetInBytes,
						 IMemoryBlock *src, size_t srcOffsetInBytes, size_t sizeInBytes);
};
