
#pragma once

#include "Definitions.h"

#include "MemoryFactory.h"

//Represents 3-dimensional region that bounds sub-grid.
class Region
{
	private:
		size_t _offsetX, _offsetY, _offsetZ;
		size_t _sizeX, _sizeY, _sizeZ;

	public:
		inline Region(size_t offsetX, size_t offsetY, size_t offsetZ,
					  size_t sizeX, size_t sizeY, size_t sizeZ)
			: _offsetX(offsetX), _offsetY(offsetY), _offsetZ(offsetZ),
			  _sizeX(sizeX), _sizeY(sizeY), _sizeZ(sizeZ)
		{ /*nothing*/ }

		__host__ __device__ inline size_t OffsetX()
		{ return _offsetX; }

		__host__ __device__ inline size_t SizeX()
		{ return _sizeX; }

		__host__ __device__ inline size_t OffsetY()
		{ return _offsetY; }

		__host__ __device__ inline size_t SizeY()
		{ return _sizeY; }

		__host__ __device__ inline size_t OffsetZ()
		{ return _offsetZ; }

		__host__ __device__ inline size_t SizeZ()
		{ return _sizeZ; }
};

//Represents grid as 3D array with meta information and access helper functions.
template<class T>
class Grid
{
	public:
		class Enumerator
		{
			private:
				Grid<T> *_owner;
				size_t _x, _y, _z;
				bool _started;

			private:
				inline Enumerator(Grid<T> *owner)
					: _owner(owner), _x(0), _y(0), _z(0), _started(false)
				{ Reset(); }

			public:
				inline size_t X() const
				{ return _x; }

				inline size_t Y() const
				{ return _y; }

				inline size_t Z() const
				{ return _z; }

				inline size_t Index() const
				{ return _owner->ComputeIndex(_x, _y, _z); }

				inline T &Value()
				{ return _owner->Pointer()[Index()]; }

				inline const T &Value() const
				{ return _owner->Pointer()[Index()]; }

				inline void Reset()
				{
					_x = 0; _y = 0; _z = 0;
					_started = false;
				}

				inline bool MoveNext()
				{
					if (!_started)
					{
						_started = true;
						return _owner->SizeX() != 0 && _owner->SizeY() != 0 && _owner->SizeZ() != 0;
					}

					if (_z >= _owner->SizeZ())
						return false;

					_x += 1;
					if (_x >= _owner->SizeX())
					{
						_x = 0;
						_y += 1;
						if (_y >= _owner->SizeY())
						{
							_y = 0;
							_z += 1;
							if (_z >= _owner->SizeZ())
								return false;
						}
					}
					return true;
				}

			friend class Grid<T>;
		};

	private:
		IMemoryBlock *_block;
		size_t _sizeX, _sizeY, _sizeZ;
		size_t _pitchYinElements, _pitchZinElements;

	private:
		inline Grid(IMemoryBlock *block,
					size_t sizeX, size_t sizeY, size_t sizeZ,
					size_t pitchYinElements, size_t pitchZinElements)
			 : _block(block), _sizeX(sizeX), _sizeY(sizeY), _sizeZ(sizeZ),
			   _pitchYinElements(pitchYinElements), _pitchZinElements(pitchZinElements)
		{ /*nothing*/ }

		inline Grid(const Grid<T> &)
		{ throw std::runtime_error("Ata-ta"); }

		inline void operator =(const Grid<T> &)
		{ throw std::runtime_error("Ata-ta"); }

	public:
		inline Enumerator GetEnumerator()
		{ return Enumerator(this); }

		inline size_t SizeX() const
		{ return _sizeX; }

		inline size_t SizeY() const
		{ return _sizeY; }

		inline size_t SizeZ() const
		{ return _sizeZ; }

		__host__ __device__ inline size_t PitchY() const
		{ return _pitchYinElements; }

		__host__ __device__ inline size_t PitchZ() const
		{ return _pitchZinElements; }

		inline MemoryType::Type MemoryType() const
		{ return _block->MemoryType(); }

		inline size_t MemorySize() const
		{ return _block->Size(); }

		__host__ __device__ inline T *Pointer() const
		{ return (T *)_block->Pointer(); }

		__host__ __device__ inline size_t ComputeIndex(size_t x, size_t y, size_t z) const
		{ return x + y * _pitchYinElements + z * _pitchZinElements; }

		inline void Clear()
		{ _block->Clear(); }

		static inline Grid *Create(MemoryType::Type memType, size_t sizeX, size_t sizeY, size_t sizeZ)
		{
			return new Grid<T>(MemoryFactory::Allocate(memType, sizeX * sizeY * sizeZ * sizeof(T)),
							   sizeX, sizeY, sizeZ, sizeX, sizeX * sizeY);
		}

		static void Copy(Grid<T> *dst, size_t dstOffsetX, size_t dstOffsetY, size_t dstOffsetZ,
						 Grid<T> *src, Region srcRegion)
		{
			T *dstP = dst->Pointer();
			T *srcP = src->Pointer();
			size_t pitchY = dst->PitchY();
			size_t pitchZ = dst->PitchZ();

			if (dst->SizeX() < dstOffsetX + srcRegion.SizeX() ||
				dst->SizeY() < dstOffsetY + srcRegion.SizeY() ||
				dst->SizeZ() < dstOffsetZ + srcRegion.SizeZ() ||
				src->SizeX() < srcRegion.OffsetX() + srcRegion.SizeX() ||
				src->SizeY() < srcRegion.OffsetY() + srcRegion.SizeY() ||
				src->SizeZ() < srcRegion.OffsetZ() + srcRegion.SizeZ())
				throw std::runtime_error("Wrong setup for grid copying");

			if (dst->MemoryType() == MemoryType::Device &&
				src->MemoryType() != MemoryType::Device)
				//TODO
				cudaMemcpy(dst->Pointer(), src->Pointer(), sizeof(T)*src->SizeX()*src->SizeY()*src->SizeZ(), cudaMemcpyHostToDevice);
			else if(dst->MemoryType() != MemoryType::Device &&
				src->MemoryType() == MemoryType::Device)
				//TODO
				cudaMemcpy(dst->Pointer(), src->Pointer(), sizeof(T)*src->SizeX()*src->SizeY()*src->SizeZ(), cudaMemcpyDeviceToHost);
			else
				for (size_t z = srcRegion.OffsetZ(); z < srcRegion.OffsetZ() + srcRegion.SizeZ(); z++)
					for (size_t y = srcRegion.OffsetY(); y < srcRegion.OffsetY() + srcRegion.SizeY(); y++)
						for (size_t x = srcRegion.OffsetX(); x < srcRegion.OffsetX() + srcRegion.SizeX(); x++)
						{
							size_t index = dst->ComputeIndex(x, y, z);
							dstP[index] = srcP[index];
						}
		}

		template <class T1, class T2>
		static inline bool HaveSameIndices(Grid<T1> *grid1, Grid<T2> *grid2)
		{
			return grid1->SizeX() == grid2->SizeX() && grid1->SizeY() == grid2->SizeY() && grid1->SizeZ() == grid2->SizeZ() &&
				   grid1->PitchY() == grid2->PitchY() && grid1->PitchZ() == grid2->PitchZ();
		}

		inline ~Grid()
		{
			if (_block != NULL)
				delete _block;
			_block = NULL;
		}
};
