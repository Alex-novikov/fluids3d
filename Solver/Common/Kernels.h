
#pragma once

#include "Definitions.h"
#include "Grid.h"
#include "CompressedIndices.h"
#include "CompressedValues.h"

class IPoissonKernel
{
	public:
		virtual void ApplyDirichletBoundary(Grid<real> *layer, Grid<int> *mask,
								   CompressedIndices *boundary,
								   CompressedValues<int> *boundaryType,
								   CompressedValues<real> *dirichletValues,
								   Region region) = 0;

		virtual void ApplyNeumannBoundary(Grid<real> *layer, Grid<int> *mask,
									CompressedIndices *boundary,
									CompressedValues<int> *boundaryType,
									CompressedValues<real> *neumannValues,
									CompressedValues<real> *neumannXValues,
									CompressedValues<real> *neumannYValues,
									CompressedValues<real> *neumannZValues,
									real dh,
									Region region) = 0; 


		virtual void DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
			Grid<real> *f, real dh,
			Grid<int> *mask, Region region) = 0;

		virtual real CalculateResidual(Grid<real> *residual, Grid<real> *srcLayer,
			Grid<int> *mask, Region region) = 0;
};


