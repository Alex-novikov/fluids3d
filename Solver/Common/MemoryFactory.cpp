
#include "MemoryFactory.h"
#include "../Helper.h"
//-----------------------
//--- HostMemoryBlock ---
//-----------------------

class HostMemoryBlock : public IMemoryBlock
{
	private:
		static inline void *Alloc(size_t sizeInBytes)
		{ return malloc(sizeInBytes); }

	public:
		HostMemoryBlock(size_t sizeInBytes)
			: IMemoryBlock(Alloc(sizeInBytes), MemoryType::Host, sizeInBytes)
		{ /*nothing*/ }

		virtual void Clear()
		{ memset(Pointer(), 0, Size()); }

		virtual ~HostMemoryBlock()
		{
			if (Pointer() != NULL)
				free(Pointer());
		}
};

//-----------------------
//--- MappedHostMemoryBlock ---
//-----------------------

class MappedHostMemoryBlock : public IMemoryBlock
{
private:
	static inline void *Alloc(size_t sizeInBytes)
	{
		void *ptr;
		Helper::cudaCheckError(cudaMallocHost((void **)&ptr, sizeInBytes, cudaHostAllocMapped));
	
		return ptr;
	}

public:
	MappedHostMemoryBlock(size_t sizeInBytes)
		: IMemoryBlock(Alloc(sizeInBytes), MemoryType::MappedHost, sizeInBytes)
	{ /*nothing*/
	}

	virtual void Clear()
	{
		memset(Pointer(), 0, Size());
	}

	virtual ~MappedHostMemoryBlock()
	{
		if (Pointer() != NULL)
			Helper::cudaCheckError(cudaFreeHost(Pointer()));
	}
};

//-----------------------
//--- DeviceMemoryBlock ---
//-----------------------

class DeviceMemoryBlock : public IMemoryBlock
{
private:
	static inline void *Alloc(size_t sizeInBytes)
	{
		void *ptr;
		Helper::cudaCheckError(cudaMalloc((void **)&ptr, sizeInBytes));

		return ptr;
	}

public:
	DeviceMemoryBlock(size_t sizeInBytes)
		: IMemoryBlock(Alloc(sizeInBytes), MemoryType::Device, sizeInBytes)
	{ /*nothing*/
	}

	virtual void Clear()
	{
		cudaMemset(Pointer(), 0, Size());
	}

	virtual ~DeviceMemoryBlock()
	{
		if (Pointer() != NULL)
			Helper::cudaCheckError(cudaFree(Pointer()));
	}
};
//---------------------
//--- MemoryFactory ---
//---------------------

IMemoryBlock *MemoryFactory::Allocate(MemoryType::Type memType, size_t sizeInBytes)
{
	if (memType == MemoryType::Host)
	{ return new HostMemoryBlock(sizeInBytes); }
	else if (memType == MemoryType::MappedHost) 
	{ return new MappedHostMemoryBlock(sizeInBytes);}
	else { return new DeviceMemoryBlock(sizeInBytes); }
}

void MemoryFactory::Copy(IMemoryBlock *dst, size_t dstOffsetInBytes,
						 IMemoryBlock *src, size_t srcOffsetInBytes, size_t sizeInBytes)
{
	if (dst->Size() < dstOffsetInBytes + sizeInBytes ||
		src->Size() < srcOffsetInBytes + sizeInBytes)
		throw std::runtime_error("Wrong setup for memory block copying");

	if (dst->MemoryType() != MemoryType::Host ||
		src->MemoryType() != MemoryType::Host)
	{
		memcpy((unsigned char *)dst->Pointer() + dstOffsetInBytes,
			   (unsigned char *)src->Pointer() + srcOffsetInBytes,
			   sizeInBytes);
	}
	else
		throw std::runtime_error("NIY");
}
