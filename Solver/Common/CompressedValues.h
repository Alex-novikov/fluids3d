
#pragma once

#include "Definitions.h"
#include "Grid.h"
#include "CompressedIndices.h"

template <class T>
class CompressedValues
{
	private:
		IMemoryBlock *_values;
		size_t _count;
		size_t _sizeX, _sizeY, _sizeZ;

	private:
		inline CompressedValues(IMemoryBlock *values, size_t count,
								size_t sizeX, size_t sizeY, size_t sizeZ)
			: _values(values), _count(count), _sizeX(sizeX), _sizeY(sizeY), _sizeZ(sizeZ)
		{ /*nothing*/ }

		inline CompressedValues(const CompressedValues &)
		{ throw std::runtime_error("Ata-ta"); }

		inline void operator =(const CompressedValues &)
		{ throw std::runtime_error("Ata-ta"); }

	public:
		__host__ __device__ inline T *Pointer() const
		{ return (T *)_values->Pointer(); }

		inline size_t Count() const
		{ return _count; }

		inline MemoryType::Type MemoryType() const
		{ return _values->MemoryType(); }

		inline size_t MemorySize() const
		{ return _values->Size(); }

		inline bool IsCompatibleWith(CompressedIndices *indices) const
		{
			return indices->SizeX() == _sizeX &&
				   indices->SizeY() == _sizeY &&
				   indices->SizeZ() == _sizeZ &&
				   indices->Count() == _count;
		}

		static CompressedValues<T> *Create(Grid<T> *grid, CompressedIndices *indices, MemoryType::Type memType = MemoryType::Host);

		~CompressedValues()
		{
			if (_values != NULL)
				delete _values;
			_values = NULL;
		}
};

template <class T>
CompressedValues<T> *CompressedValues<T>::Create(Grid<T> *grid, CompressedIndices *indices, MemoryType::Type memType)
{
	if (indices->MemoryType()==MemoryType::Device)
		throw std::runtime_error("NIY");

	if (!indices->IsCompatibleWith(grid))
		throw std::runtime_error("Cannot compress values - wrong indices");

	IMemoryBlock *_values = MemoryFactory::Allocate((memType==MemoryType::Device)?MemoryType::Host:memType, indices->Count() * sizeof(T));
	T *values = (T *)_values->Pointer();
	for (size_t z = 0; z < indices->SizeZ(); z++)
		for (int yID = indices->ZOffsets()[z]; yID < indices->ZOffsets()[z + 1]; yID++)
		{
			size_t y = indices->YIndices()[yID];
			for (int xID = indices->YOffsets()[yID]; xID < indices->YOffsets()[yID + 1]; xID++)
			{
				size_t x = indices->XIndices()[xID];
				size_t index = grid->ComputeIndex(x, y, z);
				values[xID] = grid->Pointer()[index];
			}
		}


		if (memType == MemoryType::Device){
			IMemoryBlock *_GPUValues = MemoryFactory::Allocate(MemoryType::Host, indices->Count() * sizeof(T));
			cudaMemcpy(_GPUValues->Pointer(), _values->Pointer(), indices->Count() * sizeof(T), cudaMemcpyHostToDevice);
			delete _values;
			return new CompressedValues<T>(_GPUValues, indices->Count(), indices->SizeX(), indices->SizeY(), indices->SizeZ());
		}
		else{
			return new CompressedValues<T>(_values, indices->Count(), indices->SizeX(), indices->SizeY(), indices->SizeZ());
		}
}
