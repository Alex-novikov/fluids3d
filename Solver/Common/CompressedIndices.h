
#pragma once

#include "Definitions.h"
#include "Grid.h"

class CompressedIndices
{
	public:
		class Enumerator
		{
			private:
				CompressedIndices *_owner;
				size_t _z, _yID, _xID;

			private:
				inline Enumerator(CompressedIndices *owner)
					: _owner(owner), _z(0), _yID(0), _xID(0)
				{ Reset(); }

			public:
				inline int X() const
				{ return _owner->XIndices()[_xID]; }

				inline int Y() const
				{ return _owner->YIndices()[_yID]; }
				
				inline int Z() const
				{ return (int)_z; }
				
				inline size_t Index() const
				{ return _xID; }

				void Reset();

				bool MoveNext();

			friend class CompressedIndices;
		};

	private:
		size_t _count;
		size_t _sizeX, _sizeY, _sizeZ;
		IMemoryBlock *_xIndices;			//contains x-coordinates of non-zero elements, size is equal to _nonZero value
		IMemoryBlock *_yIndices;			//contains y-coordinates of non-zero elements
		IMemoryBlock *_yOffsets;			//contains offsets from which y-th row begins
		IMemoryBlock *_zOffsets;			//contains offsets from which z-th layer begins

	private:
		inline CompressedIndices(size_t count,
								 size_t sizeX, size_t sizeY, size_t sizeZ,
								 IMemoryBlock *xIndices, IMemoryBlock *yIndices, IMemoryBlock *yOffsets, IMemoryBlock *zOffsets)
			: _count(count), _sizeX(sizeX), _sizeY(sizeY), _sizeZ(sizeZ),
			  _xIndices(xIndices), _yIndices(yIndices), _yOffsets(yOffsets), _zOffsets(zOffsets)
		{ /*nothing*/ }

		inline CompressedIndices(const CompressedIndices &)
		{ throw std::runtime_error("Ata-ta"); }

		inline void operator =(const CompressedIndices &)
		{ throw std::runtime_error("Ata-ta"); }

	public:
		inline Enumerator GetEnumerator()
		{ return Enumerator(this); }

		inline size_t Count() const
		{ return _count; }

		inline size_t SizeX() const
		{ return _sizeX; }

		inline size_t SizeY() const
		{ return _sizeY; }

		inline size_t SizeZ() const
		{ return _sizeZ; }

		__host__ __device__ inline int *XIndices() const
		{ return (int *)_xIndices->Pointer(); }

		__host__ __device__ inline int *YIndices() const
		{ return (int *)_yIndices->Pointer(); }

		__host__ __device__ inline offset *YOffsets() const
		{ return (offset *)_yOffsets->Pointer(); }

		__host__ __device__ inline offset *ZOffsets() const
		{ return (offset *)_zOffsets->Pointer(); }

		inline MemoryType::Type MemoryType() const
		{ return _xIndices->MemoryType(); }

		inline size_t MemorySize() const
		{ return _xIndices->Size() + _yIndices->Size() + _yOffsets->Size() + _zOffsets->Size(); }

		template <class T>
		bool IsCompatibleWith(Grid<T> *grid);

		template <class T>
		static CompressedIndices *Create(Grid<T> *grid, bool (*selector)(T value), MemoryType::Type memType = MemoryType::Host);

		~CompressedIndices();
};

template <class T>
CompressedIndices *CompressedIndices::Create(Grid<T> *grid, bool (*selector)(T value), MemoryType::Type memType)
{
	if (grid->MemoryType() == MemoryType::Device)
	{ throw std::runtime_error("NIY"); }

	T *ptr = grid->Pointer();
	size_t valCount = 0;
	size_t yIdCount = 0;

	//Phase 1. Counting selected cells and deteccting array sizes.
	for (size_t z = 0; z < grid->SizeZ(); z++)
		for (size_t y = 0; y < grid->SizeY(); y++)
		{
			size_t yTmp = valCount;
			for (size_t x = 0; x < grid->SizeX(); x++)
			{
				int index = grid->ComputeIndex(x, y, z);
				if (selector(ptr[index]))
				{
					valCount += 1;
				}
			}
			if (yTmp != valCount)
				yIdCount += 1;
		}

		//Phase 2. Allocating and initializing arrays.
		size_t curXId = 0;
		size_t curYId = 0;
		IMemoryBlock *_xIndices,*_yIndices;
		IMemoryBlock *_yOffsets, *_zOffsets;
		if (memType == MemoryType::MappedHost){
			_xIndices = MemoryFactory::Allocate(memType, valCount * sizeof(int));
			_yIndices = MemoryFactory::Allocate(memType, yIdCount * sizeof(int));
			_yOffsets = MemoryFactory::Allocate(memType, (yIdCount + 1) * sizeof(offset));
			_zOffsets = MemoryFactory::Allocate(memType, (grid->SizeZ() + 1) * sizeof(offset));
		}
		else {
			_xIndices = MemoryFactory::Allocate(MemoryType::Host, valCount * sizeof(int));
			_yIndices = MemoryFactory::Allocate(MemoryType::Host, yIdCount * sizeof(int));
			_yOffsets = MemoryFactory::Allocate(MemoryType::Host, (yIdCount + 1) * sizeof(offset));
			_zOffsets = MemoryFactory::Allocate(MemoryType::Host, (grid->SizeZ() + 1) * sizeof(offset));
		}
		int *xIndices = (int *)_xIndices->Pointer();
		int *yIndices = (int *)_yIndices->Pointer();
		offset *yOffsets = (offset *)_yOffsets->Pointer();
		offset *zOffsets = (offset *)_zOffsets->Pointer();

		for (size_t z = 0; z < grid->SizeZ(); z++)
		{
			zOffsets[z] = curYId;

			for (size_t y = 0; y < grid->SizeY(); y++)
			{
				size_t yTmp = curXId;
				
				for (size_t x = 0; x < grid->SizeX(); x++)
				{
					int index = grid->ComputeIndex(x, y, z);
					if (selector(ptr[index]))
					{
						xIndices[curXId] = x;
						curXId += 1;
					}
				}

				if (curXId != yTmp)
				{
					yOffsets[curYId] = yTmp;
					yIndices[curYId] = y;
					curYId += 1;
				}
			}
		}
		yOffsets[yIdCount] = valCount;
		zOffsets[grid->SizeZ()] = yIdCount;

		//Returning indices.
		if (memType != MemoryType::Device){
			return new CompressedIndices(valCount, grid->SizeX(), grid->SizeY(), grid->SizeZ(),
				_xIndices, _yIndices, _yOffsets, _zOffsets);
		} else{
			IMemoryBlock *_xGPUIndices = MemoryFactory::Allocate(MemoryType::Device, valCount * sizeof(int));
			cudaMemcpy(_xGPUIndices->Pointer(), _xIndices->Pointer(), valCount * sizeof(int), cudaMemcpyHostToDevice);
			delete _xIndices;
			IMemoryBlock *_yGPUIndices = MemoryFactory::Allocate(MemoryType::Device, yIdCount * sizeof(int));
			cudaMemcpy(_yGPUIndices->Pointer(), _yIndices->Pointer(), yIdCount * sizeof(int), cudaMemcpyHostToDevice);
			delete _yIndices;
			IMemoryBlock *_yGPUOffsets = MemoryFactory::Allocate(MemoryType::Device, (yIdCount + 1) * sizeof(offset));
			cudaMemcpy(_yGPUOffsets->Pointer(), _yOffsets->Pointer(), (yIdCount + 1) * sizeof(offset), cudaMemcpyHostToDevice);
			delete _yOffsets;
			IMemoryBlock *_zGPUOffsets = MemoryFactory::Allocate(MemoryType::Device, (grid->SizeZ() + 1) * sizeof(offset));
			cudaMemcpy(_zGPUOffsets->Pointer(), _zOffsets->Pointer(), (grid->SizeZ() + 1) * sizeof(offset), cudaMemcpyHostToDevice);
			delete _zOffsets;
			return new CompressedIndices(valCount, grid->SizeX(), grid->SizeY(), grid->SizeZ(),
				_xGPUIndices, _yGPUIndices, _yGPUOffsets, _zGPUOffsets);
		}
}

template <class T>
bool CompressedIndices::IsCompatibleWith(Grid<T> *grid)
{
	return grid->SizeX() == SizeX() &&
		   grid->SizeY() == SizeY() &&
		   grid->SizeZ() == SizeZ();
}
