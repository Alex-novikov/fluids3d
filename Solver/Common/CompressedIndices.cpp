
#include "CompressedIndices.h"

//------------------
//--- Enumerator ---
//------------------



void CompressedIndices::Enumerator::Reset()
{
	_z = 0;
	
	if (_z < _owner->SizeZ())
	{
		_yID = _owner->ZOffsets()[_z];
		_xID = _owner->YOffsets()[_yID] - 1;		//bad idea to use size_t (e.g. 0 - 1 != -1), but works fine
	}
	else
	{
		_yID = 0;
		_xID = 0;
	}
}

bool CompressedIndices::Enumerator::MoveNext()
{
	if (_z >= _owner->SizeZ())
		return false;

	_xID += 1;
	if (_xID >= _owner->YOffsets()[_yID + 1])
	{
		_yID += 1;
		if (_yID >= _owner->ZOffsets()[_z + 1])
		{
			_z += 1;
			if (_z >= _owner->SizeZ())
				return false;
			_yID = _owner->ZOffsets()[_z];
		}

		_xID = _owner->YOffsets()[_yID];
	}

	return true;
}

//-------------------------
//--- CompressedIndices ---
//-------------------------

CompressedIndices::~CompressedIndices()
{
	if (_xIndices != NULL)
		delete _xIndices;
	_xIndices = NULL;

	if (_yIndices != NULL)
		delete _yIndices;
	_yIndices = NULL;

	if (_yOffsets != NULL)
		delete _yOffsets;
	_yOffsets = NULL;

	if (_zOffsets != NULL)
		delete _zOffsets;
	_zOffsets = NULL;
}
