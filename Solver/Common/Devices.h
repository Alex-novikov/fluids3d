
#pragma once

#include "Definitions.h"

//Class that allows to specify target device (from code).
class DeviceDescriptor
{
	private:
		DeviceType::Type _deviceType;
		size_t _deviceNumber;
		size_t _unitCount;

	private:
		inline DeviceDescriptor(DeviceType::Type deviceType, size_t deviceNumber, size_t unitCount)
			: _deviceType(deviceType), _deviceNumber(deviceNumber), _unitCount(unitCount)
		{ /*nothing*/ }

	public:
		inline DeviceType::Type DeviceType() const
		{ return _deviceType; }

		inline size_t DeviceNumber() const
		{ return _deviceNumber; }

		inline size_t UnitCount() const
		{ return _unitCount; }

		static inline DeviceDescriptor CPU(size_t coreCount)
		{ return DeviceDescriptor(DeviceType::CPU, 0, coreCount); }

		static inline DeviceDescriptor GPU(size_t gpuNumber)
		{ return DeviceDescriptor(DeviceType::CPU, gpuNumber, 1); }
};
