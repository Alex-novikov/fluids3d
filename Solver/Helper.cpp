#include "Helper.h"

void Helper::cudaCheckError(cudaError err){
	if (err != cudaSuccess){
		throw new std::runtime_error(std::to_string((int)err));
	}
}

