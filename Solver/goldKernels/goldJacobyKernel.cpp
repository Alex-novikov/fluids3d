
#include "goldJacobyKernel.h"

void goldJacobyKernel::ApplyDirichletBoundary(Grid<real> *layer, Grid<int> *mask,
									  CompressedIndices *boundary,
									  CompressedValues<int> *boundaryType,
									  CompressedValues<real> *dirichletValues,
									  Region region)
{
	if (!Grid<real>::HaveSameIndices(layer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!dirichletValues->IsCompatibleWith(boundary))
	{ throw std::runtime_error("Boundary values/indices are broken"); }

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
	{
		for (size_t yID = boundary->ZOffsets()[z]; yID < (size_t)boundary->ZOffsets()[z + 1]; yID++)
		{
			int y = boundary->YIndices()[yID];
			for (size_t xID = boundary->YOffsets()[yID]; xID < (size_t)boundary->YOffsets()[yID + 1]; xID++)
			{
				int x = boundary->XIndices()[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)boundaryType->Pointer()[xID];

				if (type == BoundaryCondition::Dirichlet)
					layer->Pointer()[layer->ComputeIndex(x, y, z)] = dirichletValues->Pointer()[xID];
			}
		}

	}
}

void goldJacobyKernel::ApplyNeumannBoundary(Grid<real> *layer, Grid<int> *mask,
											CompressedIndices *boundary,
											CompressedValues<int> *boundaryType,
											CompressedValues<real> *neumannValues,
											CompressedValues<real> *neumannXValues,
											CompressedValues<real> *neumannYValues,
											CompressedValues<real> *neumannZValues,
											real dh,
											Region region){
	if (!Grid<real>::HaveSameIndices(layer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!neumannValues->IsCompatibleWith(boundary))
	{
		throw std::runtime_error("Boundary values/indices are broken");
	}

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
	{
		for (size_t yID = boundary->ZOffsets()[z]; yID < (size_t)boundary->ZOffsets()[z + 1]; yID++)
		{
			int y = boundary->YIndices()[yID];
			for (size_t xID = boundary->YOffsets()[yID]; xID < (size_t)boundary->YOffsets()[yID + 1]; xID++)
			{
				int x = boundary->XIndices()[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)boundaryType->Pointer()[xID];

				//������ �������!!!
				if (type == BoundaryCondition::Neumann){
					size_t index = layer->ComputeIndex(x, y, z);

					size_t pitchY = layer->PitchY();
					size_t pitchZ = layer->PitchZ();

					real sum = -dh * neumannValues->Pointer()[xID];

					if (neumannXValues->Pointer()[xID] > 0)
						sum += neumannXValues->Pointer()[xID] * layer->Pointer()[index + 1];
					else if (neumannXValues->Pointer()[xID] < 0)
						sum -= neumannXValues->Pointer()[xID] * layer->Pointer()[index - 1];

					if (neumannYValues->Pointer()[xID] > 0)
						sum += neumannYValues->Pointer()[xID] * layer->Pointer()[index + pitchY];
					else if (neumannYValues->Pointer()[xID] < 0)
						sum -= neumannYValues->Pointer()[xID] * layer->Pointer()[index - pitchY];

					if (neumannZValues->Pointer()[xID] > 0)
						sum += neumannZValues->Pointer()[xID] * layer->Pointer()[index + pitchZ];
					else if (neumannZValues->Pointer()[xID] < 0)
						sum -= neumannZValues->Pointer()[xID] * layer->Pointer()[index - pitchZ];

					layer->Pointer()[index] = sum / (abs(neumannXValues->Pointer()[xID]) + abs(neumannYValues->Pointer()[xID]) + abs(neumannZValues->Pointer()[xID]));
				}
			}
		}
	}

};

void goldJacobyKernel::DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
									Grid<real> *f, real dh,
									Grid<int> *mask, Region region)
{
	if (!Grid<real>::HaveSameIndices(dstLayer, srcLayer) ||
		!Grid<real>::HaveSameIndices(dstLayer, mask) ||
		!Grid<real>::HaveSameIndices(dstLayer, f))
		throw std::runtime_error("Grids with different indices are not supported");

	real *dst = dstLayer->Pointer();
	real *src = srcLayer->Pointer();
	real *fnc = f->Pointer();
	int *msk = mask->Pointer();
	size_t pitchY = mask->PitchY();
	size_t pitchZ = mask->PitchZ();
	real dhdh = dh * dh;

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
		for (size_t y = region.OffsetY(); y < region.OffsetY() + region.SizeY(); y++)
			for (size_t x = region.OffsetX(); x < region.OffsetX() + region.SizeX(); x++)
			{
				size_t index = mask->ComputeIndex(x, y, z);
				if ((CellType::Type)msk[index] == CellType::Working)
				{
					dst[index] = (src[index - 1] + src[index + 1] +
								  src[index - pitchY] + src[index + pitchY] +
								  src[index - pitchZ] + src[index + pitchZ] -
								  fnc[index] * dhdh) / 6;
				}
			}
}

real goldJacobyKernel::CalculateResidual(Grid<real> *dstLayer, Grid<real> *srcLayer, Grid<int> *mask, Region region)
{
	if (!Grid<real>::HaveSameIndices(dstLayer, srcLayer) ||
		!Grid<real>::HaveSameIndices(dstLayer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	//Checking.
	real maxDist = (real)0.0;

	for (size_t z = 0; z < region.SizeZ(); z++)
		for (size_t y = 0; y < region.SizeY(); y++)
			for (size_t x = 0; x < region.SizeX(); x++)
			{
				size_t index = srcLayer->ComputeIndex(region.OffsetX() + x,
													  region.OffsetY() + y,
												      region.OffsetZ() + z);

				if (mask->Pointer()[index] != (int)CellType::Dead)
				{
					real dist = std::abs(dstLayer->Pointer()[index] - srcLayer->Pointer()[index]);
					if (dist > maxDist)
						maxDist = dist;
				}
			}

	return maxDist;
}
