#pragma once


#include  "Common/Kernels.h"

class goldMultigridKernel :
	public IPoissonKernel
{
public:
	virtual void ApplyBoundary(Grid<real> *layer, Grid<int> *mask,
		CompressedIndices *boundary,
		CompressedValues<int> *boundaryType,
		CompressedValues<real> *dirichletValues,
		CompressedValues<real> *neumannValues,
		Region region);

	virtual void DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
		Grid<real> **residual, Grid<real> **temp,
		Grid<real> *f, real dh, int32 alt,
		Grid<int> **mask, Region** region);

	virtual void CalculateResidual(Grid<real> *residual, Grid<real> *srcLayer, 
								   Grid<real> *f, real dh, 
								   Grid<int> *mask, Region region);

	inline void NextGridInterpolation(Grid<real>* dstLayer, Grid<real>* srcLayer,
		Grid<int> *dstMask, Grid<int> *srcMask,
		Region dstRegion);

private:
	inline void NextGridSum(Grid<real>* dstLayer, Grid<real>* srcLayer,
						    Grid<int> *dstMask, Grid<int> *srcMask,
							Region dstRegion);

	inline void JacobyMethod(Grid<real> *dstLayer, Grid<real> *srcLayer,
		Grid<real> *f, real dh,
		Grid<int> *mask, Region region);

	inline void ResidualRestriction(Grid<real> *residual,
		Grid<int> *dstMask, Grid<int> *srcMask,
		Region region);

	inline void ResetBoundary(Grid<real> *layer, Grid<int> *mask,
		CompressedIndices *boundary,
		CompressedValues<int> *boundaryType,
		CompressedValues<real> *dirichletValues,
		CompressedValues<real> *neumannValues,
		Region region);
};

