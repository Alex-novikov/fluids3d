
#pragma once

#include "../Common/Kernels.h"

class goldJacobyKernel : public IPoissonKernel
{
	public:
		virtual void ApplyDirichletBoundary(Grid<real> *layer, Grid<int> *mask,
								   CompressedIndices *boundary,
								   CompressedValues<int> *boundaryType,
								   CompressedValues<real> *dirichletValues,
								   Region region);

		virtual void ApplyNeumannBoundary(Grid<real> *layer, Grid<int> *mask,
			CompressedIndices *boundary,
			CompressedValues<int> *boundaryType,
			CompressedValues<real> *neumannValues,
			CompressedValues<real> *neumannXValues,
			CompressedValues<real> *neumannYValues,
			CompressedValues<real> *neumannZValues,
			real dh,
			Region region);

		virtual void DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
			Grid<real> *f, real dh,
			Grid<int> *mask, Region region);

		virtual real CalculateResidual(Grid<real> *residual, Grid<real> *srcLayer,
			Grid<int> *mask, Region region);
};
