#include "goldMultigridKernel.h"
//it must be more than zero
#define SMOOTH_ITERATIONS 3

void goldMultigridKernel::ApplyBoundary(Grid<real> *layer, Grid<int> *mask,
	CompressedIndices *boundary,
	CompressedValues<int> *boundaryType,
	CompressedValues<real> *dirichletValues,
	CompressedValues<real> *neumannValues,
	Region region)
{
	if (!Grid<real>::HaveSameIndices(layer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!dirichletValues->IsCompatibleWith(boundary) ||
		!neumannValues->IsCompatibleWith(boundary))
	{
		throw std::runtime_error("Boundary values/indices are broken");
	}

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
	{
		for (size_t yID = boundary->ZOffsets()[z]; yID < (size_t)boundary->ZOffsets()[z + 1]; yID++)
		{
			int y = boundary->YIndices()[yID];
			for (size_t xID = boundary->YOffsets()[yID]; xID < (size_t)boundary->YOffsets()[yID + 1]; xID++)
			{
				int x = boundary->XIndices()[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)boundaryType->Pointer()[xID];

				if (type == BoundaryCondition::Dirichlet)
					layer->Pointer()[layer->ComputeIndex(x, y, z)] = dirichletValues->Pointer()[xID];
				else
					throw std::runtime_error("NIY");
			}
		}

	}
}


//���, �������, ����� ���� ������ ������� �������
void goldMultigridKernel::ResetBoundary(Grid<real> *layer, Grid<int> *mask,
	CompressedIndices *boundary,
	CompressedValues<int> *boundaryType,
	CompressedValues<real> *dirichletValues,
	CompressedValues<real> *neumannValues,
	Region region)
{
	if (!Grid<real>::HaveSameIndices(layer, mask))
		throw std::runtime_error("Grids with different indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!dirichletValues->IsCompatibleWith(boundary) ||
		!neumannValues->IsCompatibleWith(boundary))
	{
		throw std::runtime_error("Boundary values/indices are broken");
	}

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
	{
		for (size_t yID = boundary->ZOffsets()[z]; yID < (size_t)boundary->ZOffsets()[z + 1]; yID++)
		{
			int y = boundary->YIndices()[yID];
			for (size_t xID = boundary->YOffsets()[yID]; xID < (size_t)boundary->YOffsets()[yID + 1]; xID++)
			{
				int x = boundary->XIndices()[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)boundaryType->Pointer()[xID];

				if (type == BoundaryCondition::Dirichlet)
					layer->Pointer()[layer->ComputeIndex(x, y, z)] = 0;
				else
					throw std::runtime_error("NIY");
			}
		}

	}
}

//�������� � ������������ ����������
inline void goldMultigridKernel::NextGridSum(Grid<real>* dstLayer, Grid<real>* srcLayer,
											  Grid<int> *dstMask, Grid<int> *srcMask,
											  Region dstRegion)
{
	real *dst = dstLayer->Pointer();
	real *src = srcLayer->Pointer();
	int *dMsk = dstMask->Pointer();
	size_t pitchY = dstMask->PitchY();
	size_t pitchZ = dstMask->PitchZ();

	for (size_t z = dstRegion.OffsetZ(); z < dstRegion.OffsetZ() + dstRegion.SizeZ(); z++)
		for (size_t y = dstRegion.OffsetY(); y < dstRegion.OffsetY() + dstRegion.SizeY(); y++)
			for (size_t x = dstRegion.OffsetX(); x < dstRegion.OffsetX() + dstRegion.SizeX(); x++)
			{
				size_t srcIndex = srcMask->ComputeIndex(x / 2, y / 2, z / 2);
				size_t dstIndex = dstMask->ComputeIndex(x, y, z);
				// ���������������, ��� ��� ���������� ����������� dead � working ��������� � working
				if ((CellType::Type)dMsk[dstIndex] == CellType::Working)
				{
					dst[dstIndex] += src[srcIndex];
				}
			}
}

inline void goldMultigridKernel::JacobyMethod(Grid<real> *dstLayer, Grid<real> *srcLayer,
	Grid<real> *f, real dh,
	Grid<int> *mask, Region region)
{
	real *dst = dstLayer->Pointer();
	real *src = srcLayer->Pointer();
	real *fnc = f->Pointer();
	int *msk = mask->Pointer();
	size_t pitchY = mask->PitchY();
	size_t pitchZ = mask->PitchZ();
	real dhdh = dh * dh;

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
		for (size_t y = region.OffsetY(); y < region.OffsetY() + region.SizeY(); y++)
			for (size_t x = region.OffsetX(); x < region.OffsetX() + region.SizeX(); x++)
			{
				size_t index = mask->ComputeIndex(x, y, z);
				if ((CellType::Type)msk[index] == CellType::Working)
				{
					dst[index] = (src[index - 1] + src[index + 1] +
						src[index - pitchY] + src[index + pitchY] +
						src[index - pitchZ] + src[index + pitchZ] -
						fnc[index] * dhdh) / 6;
				}
			}
}

//��� ����� ���������� ������ ��� ������������!!!
//+ �������� � ������!
inline void goldMultigridKernel::ResidualRestriction(Grid<real> *residual,
													  Grid<int> *dstMask, Grid<int> *srcMask, 
													  Region region)
{
	real *rsdl = residual->Pointer();
	int *msk = dstMask->Pointer();
	size_t pitchY = srcMask->PitchY();
	size_t pitchZ = srcMask->PitchZ();

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ() - 2; z++)
		for (size_t y = region.OffsetY(); y < region.OffsetY() + region.SizeY() - 2; y++)
			for (size_t x = region.OffsetX(); x < region.OffsetX() + region.SizeX() - 2; x++)
			{
				//��� �� ����� � �� �� ����� �����, ������� ����� ������ �� ���������
				size_t dIndex = srcMask->ComputeIndex(x/2, y/2, z/2);
				size_t sIndex = srcMask->ComputeIndex(x, y, z);
				//�� ���������� �� ���� ������� ��� �� ������ ����
				if ((CellType::Type)msk[dstMask->ComputeIndex(x / 2, y / 2, z / 2)] == CellType::Working)
				{
					rsdl[dIndex] = (rsdl[sIndex] + rsdl[sIndex + 1] +
						rsdl[sIndex + pitchY] + rsdl[sIndex + pitchY + 1] +
						rsdl[sIndex + pitchZ] + rsdl[sIndex + pitchZ + 1] +
						rsdl[sIndex + pitchY + pitchZ] + rsdl[sIndex + pitchY + pitchZ + 1]) / 8;
				}
			}
		//���� ������� ��������� �������
		//������ ���� ���
		//for (int i = 0; i < y / step / 2 + 1; i++){
		//	residual[((int)pow(2, alt) + 2)*(1 + i) + x / step / 2 + 1] = 0;
		//	residual[((int)pow(2, alt) + 2)*(x / step / 2 + 1) + 1 + i] = 0;
		//}
		//������, �����, ������� �����
};
	
///<summary>V-cycle method</summary>
///<param name="dstLayer">Must have applied boundary</param>
void goldMultigridKernel::DoIteration(Grid<real> *dstLayer, Grid<real> *srcLayer,
	Grid<real> **residual, Grid<real> **temp,
	Grid<real> *f, real dh, int32 alt,
	Grid<int> **mask, Region** region)
{
	if (!Grid<real>::HaveSameIndices(dstLayer, srcLayer) ||
		!Grid<real>::HaveSameIndices(dstLayer, mask[alt]) ||
		!Grid<real>::HaveSameIndices(dstLayer, f))
		throw std::runtime_error("Grids with different indices are not supported");

	real* ptr;
	//pre-smoothing
	JacobyMethod(temp[alt], srcLayer, f, dh, mask[alt], *region[alt]);
	for (int n = 1; n < SMOOTH_ITERATIONS; n++){
		JacobyMethod(temp[alt], temp[alt], f, dh, mask[alt], *region[alt]);
	}

	//moving down
	real tdh = dh;
	CalculateResidual(residual[alt-1], temp[alt], f, tdh, mask[alt], *region[alt]);
	ResidualRestriction(residual[alt-1], mask[alt-1], mask[alt], *region[alt]);
	temp[alt - 1]->Clear();
	JacobyMethod(temp[alt-1], temp[alt-1], residual[alt-1], 2*tdh, mask[alt-1], *region[alt-1]);
	
	int32 talt = alt;
	while(talt > 2) {
		talt--; tdh *= 2;
		CalculateResidual(residual[talt - 1], temp[talt], residual[talt], tdh, mask[talt], *region[talt]);
		ResidualRestriction(residual[talt - 1], mask[talt - 1], mask[talt], *region[talt]);
		temp[talt - 1]->Clear();
		JacobyMethod(temp[talt - 1], temp[talt - 1], residual[talt - 1], 2 * tdh, mask[talt-1], *region[talt-1]);
	} 

	//moving up
	while (talt < alt - 1){
		//u+ interpolated v
		NextGridSum(temp[talt+1], temp[talt], mask[talt+1], mask[talt], *region[talt+1]);

		//post-smoothing
		for (int n = 0; n < SMOOTH_ITERATIONS; n++)
			JacobyMethod(temp[talt + 1], temp[talt + 1], residual[talt + 1], tdh, mask[talt + 1], *region[talt + 1]);
		talt++; tdh /= 2;
	}

	//u + interpolated v
	NextGridSum(dstLayer, temp[talt - 1], mask[talt], mask[talt - 1], *region[talt]);

	//post-smoothing
	for (int n = 0; n < SMOOTH_ITERATIONS; n++){
		JacobyMethod(dstLayer, dstLayer, residual[alt], dh, mask[talt], *region[talt]);
	}
}

void goldMultigridKernel::CalculateResidual(Grid<real> *residual, Grid<real> *srcLayer, 
											Grid<real> *f, real dh,
											Grid<int> *mask, Region region)
{
	real *dst = residual->Pointer();
	real *src = srcLayer->Pointer();
	real *fnc = f->Pointer();
	int *msk = mask->Pointer();
	size_t pitchY = mask->PitchY();
	size_t pitchZ = mask->PitchZ();
	real dhdh = dh * dh;

	for (size_t z = region.OffsetZ(); z < region.OffsetZ() + region.SizeZ(); z++)
		for (size_t y = region.OffsetY(); y < region.OffsetY() + region.SizeY(); y++)
			for (size_t x = region.OffsetX(); x < region.OffsetX() + region.SizeX(); x++)
			{
				size_t index = mask->ComputeIndex(x, y, z);
				if ((CellType::Type)msk[index] == CellType::Working)
				{
					dst[index] = (src[index - 1] + src[index + 1] +
						src[index - pitchY] + src[index + pitchY] +
						src[index - pitchZ] + src[index + pitchZ] -
						6 * src[index])/dhdh + fnc[index];
				}
			}
}
