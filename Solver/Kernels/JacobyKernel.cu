﻿#include "Kernels/JacobyKernel.cuh"
#include "../Common/Kernels.h"

__device__ float atomicMaxf(float* address, float val)
{
	int *address_as_int = (int*)address;
	int old = *address_as_int, assumed;
	while (val > __int_as_float(old)) {
		assumed = old;
		old = atomicCAS(address_as_int, assumed,
			__float_as_int(val));
	}
	return __int_as_float(old);
}



__global__ void ApplyDirichletBoundaryKernel(
	size_t startX, size_t endX,
	size_t startY, size_t endY,
	size_t startZ, size_t endZ,
	offset* zoffsets, offset* yoffsets, int* xindices, int* yindices, int* btptr, real* dvptr, real* lptr,
	size_t pitchY, size_t pitchZ)
{	
	size_t z = threadIdx.x + blockIdx.x * blockDim.x;
	if (startZ + z < endZ)
	{
		for (size_t yID = zoffsets[z]; yID < (size_t)zoffsets[z + 1]; yID++)
		{
			int y = yindices[yID];
			for (size_t xID = yoffsets[yID]; xID < (size_t)yoffsets[yID + 1]; xID++)
			{
				int x = xindices[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)btptr[xID];
				if (type == BoundaryCondition::Dirichlet)
					lptr[x + y * pitchY + z * pitchZ] = dvptr[xID];
	
			}
		}
	}
}

__global__ void ApplyNeumannBoundaryKernel(
	size_t startZ, size_t endZ,
	real dh,
	offset* zoffsets, offset* yoffsets, int* xindices, int* yindices, int* btptr, real* lptr,
	real *nvptr, real *nxvptr, real *nyvptr, real *nzvptr,
	size_t pitchY, size_t pitchZ){
	int z = threadIdx.x + blockIdx.x * blockDim.x;
	if (startZ + z < endZ)
	{
		for (size_t yID = zoffsets[z]; yID < (size_t)zoffsets[z + 1]; yID++)
		{
			int y = yindices[yID];
			for (size_t xID = yoffsets[yID]; xID < (size_t)yoffsets[yID + 1]; xID++)
			{
				int x = xindices[xID];
				BoundaryCondition::Type type = (BoundaryCondition::Type)btptr[xID];

				//ПЕРВЫЙ ПОРЯДОК!!!
				if (type == BoundaryCondition::Neumann){
					size_t index = x + y * pitchY + z * pitchZ;

					real sum = -dh * nvptr[xID];

					if (nxvptr[xID] > 0)
						sum += nxvptr[xID] * lptr[index + 1];
					else if (nxvptr[xID] < 0)
						sum -= nxvptr[xID] * lptr[index - 1];

					if (nyvptr[xID] > 0)
						sum += nyvptr[xID] * lptr[index + pitchY];
					else if (nyvptr[xID] < 0)
						sum -= nyvptr[xID] * lptr[index - pitchY];

					if (nzvptr[xID] > 0)
						sum += nzvptr[xID] * lptr[index + pitchZ];
					else if (nzvptr[xID] < 0)
						sum -= nzvptr[xID] * lptr[index - pitchZ];

					lptr[index] = sum / (abs(nxvptr[xID]) + abs(nyvptr[xID]) + abs(nzvptr[xID]));
				}
			}
		}
	}
};

__global__ void DoIterationKernel(
	size_t startX, size_t endX,
	size_t startY, size_t endY,
	size_t startZ, size_t endZ,
	real *dst,real *src,real *fnc, int *msk,
	real dhdh,
	size_t pitchY, size_t pitchZ)
{
	
	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (startX + x < endX)
	if (startY + y < endY)
			for (size_t z = startZ; z < endZ; z++){
				size_t index = x + y * pitchY + z * pitchZ;
				if ((CellType::Type)msk[index] == CellType::Working)
				{
					dst[index] = (src[index - 1] + src[index + 1] +
						src[index - pitchY] + src[index + pitchY] +
						src[index - pitchZ] + src[index + pitchZ] -
						fnc[index] * dhdh) / 6;
				}
			}
}

__global__ void CalculateResidualKernel(
	size_t startX, size_t endX,
	size_t startY, size_t endY,
	size_t startZ, size_t endZ, 
	real *dst, real *src, int *msk,
	size_t pitchY, size_t pitchZ, real *answer, int* mutex)
{


	//Checking.
	real maxDist = (real)0.0;

	int x = threadIdx.x + blockIdx.x * blockDim.x;
	int y = threadIdx.y + blockIdx.y * blockDim.y;

	if (startX + x < endX)
	if (startY + y < endY)
	for (size_t z = startZ; z < endZ; z++){
		size_t index = x + y * pitchY + z * pitchZ;

		if (msk[index] != (int)CellType::Dead)
		{
			real dist = std::abs(dst[index] - src[index]);
			if (dist > maxDist)
				maxDist = dist;
		}
	}

	atomicMaxf(answer, maxDist);
}
