﻿
#pragma once

#include "../Common/Kernels.h"

__global__ void ApplyDirichletBoundaryKernel(
	size_t startX, size_t endX,
	size_t startY, size_t endY,
	size_t startZ, size_t endZ,
	offset* zoffsets, offset* yoffsets, int* xindices, int* yindices, int* btptr, real* dvptr, real* lptr, size_t pitchY, size_t pitchZ);

__global__ void ApplyNeumannBoundaryKernel(
	size_t startZ, size_t endZ,
	real dh,
	offset* zoffsets, offset* yoffsets, int* xindices, int* yindices, int* btptr, real* lptr,
	real *nvptr, real *nxvptr, real *nyvptr, real *nzvptr,
	size_t pitchY, size_t pitchZ);

__global__ void DoIterationKernel(
	size_t startX, size_t endX,
	size_t startY, size_t endY,
	size_t startZ, size_t endZ,
	real *dst, real *src, real *fnc, int *msk,
	real dhdh,
	size_t pitchY, size_t pitchZ);

__global__ void CalculateResidualKernel(
	size_t startX, size_t endX,
	size_t startY, size_t endY,
	size_t startZ, size_t endZ,
	real *dst, real *src, int *msk,
	size_t pitchY, size_t pitchZ, real *answer, int* mutex);

class JacobyKernel : public IPoissonKernel
{
public:
	virtual void ApplyDirichletBoundary(Grid<real> *layer, Grid<int> *mask,
		CompressedIndices *boundary,
		CompressedValues<int> *boundaryType,
		CompressedValues<real> *dirichletValues,
		Region region){
		if (!Grid<real>::HaveSameIndices(layer, mask))
			throw std::runtime_error("Grids with different indices are not supported");

		if (!boundaryType->IsCompatibleWith(boundary) ||
			!dirichletValues->IsCompatibleWith(boundary))
		{
			throw std::runtime_error("Boundary values/indices are broken");
		}
		dim3 grid(1 + (layer->SizeZ() - 1) /256), threads(256);
		cudaError err = cudaGetLastError();

		offset *zoffsets, *yoffsets;
		int *xindices, *yindices, *btptr;
		real *dvptr, *lptr;
		zoffsets = boundary->ZOffsets();
		yoffsets = boundary->YOffsets();
		xindices = boundary->XIndices();
		yindices = boundary->YIndices();
		btptr=boundaryType->Pointer();
		dvptr=dirichletValues->Pointer();
		lptr = layer->Pointer();
		ApplyDirichletBoundaryKernel <<<grid, threads >>>(region.OffsetX(), region.OffsetX() + region.SizeX(), region.OffsetY(), region.OffsetY() + region.SizeY(), region.OffsetZ(), region.OffsetZ() + region.SizeZ(), zoffsets, yoffsets, xindices, yindices, btptr, dvptr, lptr, layer->PitchY(), layer->PitchZ());
		cudaDeviceSynchronize();
		err = cudaGetLastError();
	};

	virtual void ApplyNeumannBoundary(Grid<real> *layer, Grid<int> *mask,
		CompressedIndices *boundary,
		CompressedValues<int> *boundaryType,
		CompressedValues<real> *neumannValues,
		CompressedValues<real> *neumannXValues,
		CompressedValues<real> *neumannYValues,
		CompressedValues<real> *neumannZValues,
		real dh,
		Region region){
		if (!Grid<real>::HaveSameIndices(layer, mask))
			throw std::runtime_error("Grids with different indices are not supported");

		if (!boundaryType->IsCompatibleWith(boundary) ||
			!neumannValues->IsCompatibleWith(boundary))
		{
			throw std::runtime_error("Boundary values/indices are broken");
		}
		offset *zoffsets, *yoffsets;
		int *xindices, *yindices, *btptr;
		real *nvptr, *nxvptr, *nyvptr, *nzvptr, *lptr;
		zoffsets = boundary->ZOffsets();
		yoffsets = boundary->YOffsets();
		xindices = boundary->XIndices();
		yindices = boundary->YIndices();
		btptr = boundaryType->Pointer();
		nvptr=neumannValues->Pointer();
		nxvptr=neumannXValues->Pointer();
		nyvptr=neumannYValues->Pointer();
		nzvptr=neumannZValues->Pointer();
		lptr = layer->Pointer();
		dim3 grid(1 + (layer->SizeZ() - 1) / 256), threads(256);
		cudaError err = cudaGetLastError();
		ApplyNeumannBoundaryKernel << <grid, threads >> >(region.OffsetZ(), region.OffsetZ() + region.SizeZ(), dh, zoffsets, yoffsets, xindices, yindices, btptr, lptr, nvptr, nxvptr, nyvptr, nzvptr, layer->PitchY(), layer->PitchZ());
		err = cudaGetLastError();
		cudaDeviceSynchronize();
	};

	virtual void DoIteration(
		Grid<real> *dstLayer, Grid<real> *srcLayer,
		Grid<real> *f, real dh,
		Grid<int> *mask, Region region){
		if (!Grid<real>::HaveSameIndices(dstLayer, srcLayer) ||
			!Grid<real>::HaveSameIndices(dstLayer, mask) ||
			!Grid<real>::HaveSameIndices(dstLayer, f))
			throw std::runtime_error("Grids with different indices are not supported");
		dim3 grid( 1 + (dstLayer->SizeX() - 1) / 16, 1 + (dstLayer->SizeY() - 1) / 16), threads(16, 16);
		real *dst, *src, *fnc;
		int *msk;
		dst = dstLayer->Pointer();
		src = srcLayer->Pointer();
		fnc = f->Pointer();
		msk = mask->Pointer();
		DoIterationKernel << <grid, threads >> >(region.OffsetX(), region.OffsetX() + region.SizeX(), region.OffsetY(), region.OffsetY() + region.SizeY(), region.OffsetZ(), region.OffsetZ() + region.SizeZ(), dst, src, fnc, msk, dh*dh, mask->PitchY(), mask->PitchZ());
		cudaError err = cudaGetLastError();
		cudaDeviceSynchronize();
	};

	__host__ virtual real CalculateResidual(Grid<real> *dstLayer, Grid<real> *srcLayer,
		Grid<int> *mask, Region region){
		if (!Grid<real>::HaveSameIndices(dstLayer, srcLayer) ||
			!Grid<real>::HaveSameIndices(dstLayer, mask))
			throw std::runtime_error("Grids with different indices are not supported");
		dim3 grid(1 + (dstLayer->SizeX() - 1) / 16, 1 + (dstLayer->SizeY() - 1) / 16), threads(16, 16);
		void *curCUDAEps;
		
		real *dst, *src;
		int *msk;
		dst = dstLayer->Pointer();
		src = srcLayer->Pointer();
		msk = mask->Pointer();
		cudaMalloc(&curCUDAEps, sizeof(real)); 
		real result = 0;
		cudaMemcpy(curCUDAEps, &result, sizeof(real), cudaMemcpyHostToDevice); 
		int *mutex;
		int state = 0;
		cudaMalloc((void**)&mutex, sizeof(int));
		cudaMemcpy(mutex, &state, sizeof(int), cudaMemcpyHostToDevice);
		CalculateResidualKernel << <grid, threads >> >(region.OffsetX(), region.OffsetX() + region.SizeX(), region.OffsetY(), region.OffsetY() + region.SizeY(), region.OffsetZ(), region.OffsetZ() + region.SizeZ(), dst, src, msk, mask->PitchY(), mask->PitchZ(), (real*)curCUDAEps, mutex);
		cudaDeviceSynchronize();
		cudaMemcpy(&result, curCUDAEps, sizeof(real), cudaMemcpyDeviceToHost);
		cudaFree(curCUDAEps);
		return result;
	};
};
