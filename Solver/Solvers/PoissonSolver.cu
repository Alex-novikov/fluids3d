
#include "PoissonSolver.h"
#include "Kernels/JacobyKernel.cuh"
#include "Helper.h"

#include <cmath>
#include <ctime>
#include <iostream>

#include "mkl_rci.h"
#include "mkl_blas.h"
#include "mkl_spblas.h"
#include "mkl_service.h"

//---------------------
//--- PoissonSolver ---
//---------------------

void PoissonSolver::Configure(const std::vector<std::pair<DeviceDescriptor, float> > &usage)
{
	if (usage.size() != 1 ||
		usage[0].second != 1.0f ||
		usage[0].first.DeviceType() != DeviceType::CPU ||
		usage[0].first.UnitCount() != 1)
		throw std::runtime_error("NIY");
}

PerfInfo PoissonSolver::Solve(Grid<real> *dst, Grid<real> *fcpu, Grid<int> *maskcpu,
							  CompressedIndices *boundary,
							  CompressedValues<int> *boundaryType,
							  CompressedValues<real> *dirichletValues,
							  CompressedValues<real> *neumannValues,
							  CompressedValues<real> *nxValues,
							  CompressedValues<real> *nyValues,
							  CompressedValues<real> *nzValues,
							  real dh, real eps, size_t maxIter, DeviceType::Type type)
{
	//Checking.
	if (!Grid<real>::HaveSameIndices(dst, _buf) ||
		!Grid<real>::HaveSameIndices(dst, fcpu) ||
		!Grid<real>::HaveSameIndices(dst, maskcpu))
		throw std::runtime_error("Grids with different sizes and/or indices are not supported");

	if (!boundaryType->IsCompatibleWith(boundary) ||
		!dirichletValues->IsCompatibleWith(boundary) ||
		!neumannValues->IsCompatibleWith(boundary))
		throw std::runtime_error("Broken compressed boundary values");

	/*if (dst->MemoryType() != MemoryType::Host ||
		f->MemoryType() != MemoryType::Host ||
		mask->MemoryType() != MemoryType::Host)
		throw std::runtime_error("NIY");
		*/
	//Preparing.
	unsigned int start_time = clock();
	Region region(0,0,0,dst->SizeX(),dst->SizeY(),dst->SizeZ());
	
	Grid<real>* temp1 = Grid<real>::Create(MemoryType::Device, dst->SizeX(), dst->SizeY(), dst->SizeZ());
	Grid<real>* temp2 = Grid<real>::Create(MemoryType::Device, dst->SizeX(), dst->SizeY(), dst->SizeZ());
	Grid<real>* ttemp;
	temp1->Clear();
	temp2->Clear();

	Grid<real>* f = Grid<real>::Create(MemoryType::Device, dst->SizeX(), dst->SizeY(), dst->SizeZ());
	Grid<real>::Copy(f, 0, 0, 0, fcpu, region);

	Grid<int>* mask = Grid<int>::Create(MemoryType::Device, dst->SizeX(), dst->SizeY(), dst->SizeZ());
	Grid<int>::Copy(mask, 0, 0, 0, maskcpu, region);

	_kernel->ApplyDirichletBoundary(temp1, mask, boundary, boundaryType, dirichletValues, region);
	_kernel->ApplyDirichletBoundary(temp2, mask, boundary, boundaryType, dirichletValues, region);
	
	int n = 0;
	real curEps = 1000000000;
	//Solving.
	if (type == DeviceType::CPU){
		int count = 0;
		for (size_t z = 0; z < mask->SizeZ() - 1; z++){
			for (size_t y = 0; y < mask->SizeY() - 1; y++){
				for (size_t x = 0; x < mask->SizeX() - 1; x++){
					size_t index = mask->ComputeIndex(x, y, z);
					if ((CellType::Type)mask->Pointer()[index] != CellType::Dead)
						count++;
				}
			}
		}

		for (; (n <= maxIter) && (curEps > eps); n++){
			ttemp = temp1;
			temp1 = temp2;
			temp2 = ttemp;
			_kernel->ApplyNeumannBoundary(temp1, mask, boundary, boundaryType, neumannValues, nxValues, nyValues, nzValues, dh, region);
			_kernel->DoIteration(temp2, temp1, f, dh, mask, region);
			curEps = _kernel->CalculateResidual(temp1, temp2, mask, region);
		}
	}
	else if (type == DeviceType::GPU){

		for (; (n <= maxIter) && (curEps > eps); n++){
			if (n % 250 == 0){
				std::cout << n << " " << curEps << endl;
			}
			ttemp = temp1;
			temp1 = temp2;
			temp2 = ttemp;
			_kernel->ApplyNeumannBoundary(temp1, mask, boundary, boundaryType, neumannValues, nxValues, nyValues, nzValues, dh, region);
			_kernel->DoIteration(temp2, temp1, f, dh, mask, region);
			curEps=_kernel->CalculateResidual(temp1, temp2, mask, region);
			//curEps = *(real*)curCUDAEps;
		}
	}
	if (maxIter != 0 && n > maxIter)
		throw std::runtime_error("Failed to solve Poisson equation within required count of iterations");

	Grid<real>::Copy(dst, 0, 0, 0, temp1, region);

	delete temp1;
	delete temp2;

	PerfInfo info;
	info.AddRecord("Overall", 100, 100, (float)(clock() - start_time)/CLOCKS_PER_SEC);
	return info;

	/*
	//Solving.
	real curEps = (real)0.0;
	size_t curIter = 0;
	do
	{
		Grid<real> *cur = bufs[curBuf];
		Grid<real> *nxt = bufs[(curBuf + 1) % 2];
		curBuf = (curBuf + 1) % 2;

		for (size_t i = 0; i < 5; i++)
		{
			_kernel->DoIteration(nxt, cur, f, dh, mask, wholeGrid);
			_kernel->ApplyBoundary(nxt, mask, boundary, boundaryType, dirichletValues, neumannValues, wholeGrid);
		}
		
		curEps = _kernel->CalculateResidual(cur, nxt, mask, wholeGrid);
		curIter += 1;
	}
	while (curEps > eps && (curIter < maxIter || maxIter == 0));

	//Not solved =(
	if (maxIter != 0 && curIter >= maxIter)
		throw std::runtime_error("Failed to solve Poisson equation within required count of iterations");
	//Solved =)
	else
	{
		if (curIter % 2 == 1)
			Grid<real>::Copy(dst, 0, 0, 0, _buf, wholeGrid);

		return PerfInfo();
	}*/
}

PoissonSolver *PoissonSolver::Create(size_t sizeX, size_t sizeY, size_t sizeZ)
{
	IPoissonKernel *kernel = NULL;
	Grid<real> *buf = NULL;
	
	try
	{
		kernel = new JacobyKernel();
		buf = Grid<real>::Create(MemoryType::MappedHost, sizeX, sizeY, sizeZ);
	}
	catch (std::exception &)
	{
		if (kernel != NULL)
			delete kernel;

		if (buf != NULL)
			delete buf;
		
		throw;
	}

	return new PoissonSolver(kernel, buf);
}

inline void PoissonSolver::NextGridInterpolation(Grid<real>* dstLayer, Grid<real>* srcLayer,
												 Grid<int> *dstMask, Grid<int> *srcMask,
												 Region dstRegion)
{
	real *dst = dstLayer->Pointer();
	real *src = srcLayer->Pointer();
	int *dMsk = dstMask->Pointer();
	size_t pitchY = dstMask->PitchY();
	size_t pitchZ = dstMask->PitchZ();

	for (size_t z = dstRegion.OffsetZ() + 1; z < dstRegion.OffsetZ() + dstRegion.SizeZ() - 1; z++)
		for (size_t y = dstRegion.OffsetY() + 1; y < dstRegion.OffsetY() + dstRegion.SizeY() - 1; y++)
			for (size_t x = dstRegion.OffsetX() + 1; x < dstRegion.OffsetX() + dstRegion.SizeX() - 1; x++)
			{
				size_t srcIndex = srcMask->ComputeIndex((x+1) / 2, (y+1) / 2, (z+1) / 2);
				size_t dstIndex = dstMask->ComputeIndex(x, y, z);
				// ���������������, ��� ��� ���������� ����������� dead � working ��������� � working
				if ((CellType::Type)dMsk[dstIndex] == CellType::Working)
				{
					dst[dstIndex] = src[srcIndex];
				}
			}
}

PoissonSolver::~PoissonSolver()
{
	if (_kernel != NULL)
		delete _kernel;
	_kernel = NULL;

	if (_buf != NULL)
		delete _buf;
	_buf = NULL;
}
